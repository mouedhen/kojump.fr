# Kojump

**Description**

## Instructions d'installation

Instructions d'installation pour la distribution Ubuntu

* Installer les dépendances système

```bash
sudo apt-get install git sqlite3 make python3 python3-pip python3-venv # install application system dependencies
sudo apt-get install binutils spatialite-bin libsqlite3-mod-spatialite libproj-dev gdal-bin libgeoip1 python3-gdal # install geospacial libraries
```

* Cloner le projet et créer l'environnement virtuelle (recommandé)

```bash
git clone $PROJECT_REPOS # clone the repository
cd kojump.fr # move to the project directory
python3 -m venv .koenv3 # create a python 3 virtual environment
source .koenv3/bin/activate # activate the virtual environment
python --version # check python default interpreter (it should be python 3)
```

* Installer les dépendances de l'application

```bash
pip install -r requirements.txt
django-admin --version # check for django version (it should be 2.0.6)
```

* Créer la base de données et importer les données depuis les fichiers

Vous devez adapter la variable SPATIALITE_LIBRARY_PATH dans les fichiers de configuration (src/kojump/settings/development.py and src/kojump/settings/production.py) selon votre configuration

```bash
cd src
python manage.py migrate --settings="kojump.settings.development" # or in production python manage.py migrate --settings="kojump.settings.production"
make populate_dev --settings="kojump.settings.development" # for development settings or populate_prod for production settings
python manage.py runserver --settings="kojump.settings.development" # or in production python manage.py migrate --settings="kojump.settings.production"
```

* Collecter les fichiers statiques (seulement en production)

En production, djando sert les fichiers statique d'un seul dossier (kojump/static). Afin de regrouper tous ses fichiers sous ce dossier il faudrait exécuter la commande suivante

```bash
cd src
rm -R kojump/static # remove old files to avoid conflicts
python manage.py collectstatic --settings="kojump.settings.production" # collect all static files
```

## Pythonanywere

L'application est hébergée pour le moment sur les serveurs de pythonanywere, l'environnement utilisé est myvirtualenv, avant d'exécuter toutes commande python sous le serveur, il faudrait l'activer

```bash
workon myvirtualenv
```

## Usage

## Dernières modifications

- [x] [BUG] Corriger les liens des équipements dans la carte
- [x] [BUG] Corriger les liens des institutions dans la carte
- [x] [BUG] l'utilisateur ne peut pas laisser un commentaire sur les équipements
- [x] [BUG] lien de la section commentaires de la page institution
- [x] [BUG] notation des institutions
- [x] [BUG] notation des équipements
- [x] [BUG] l'application ne filtre pas en fonction du type de lieu (publique / privé)
- [x] [BUG] Correction de l'affichage de la map lors de l'activation du panneau de recherche avancée
- [x] [FEATURES] Simplifier les règles pour les mots de passes
- [x] [FEATURES] Ajout de fonctionnalités de gestion de contenu
- [x] [FEATURES] Prise en charge de la connexion via les réseaux sociaux
- [x] [DOCS] le symbole du pin dans le champs de recherche est pour l'identification visuelle de la fonction du champ
- [x] [DOCS] la géolocalisation des utilisateurs se fait avec le button présent sur la carte
- [x] [ALERT] La simplification des règles pour les mots de passes constitue une brèche de sécurité
- [x] [MIGRATION] Migration vers Django 2.0

## Credit

## License
