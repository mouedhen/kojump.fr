"""kojump URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.urls import path, re_path
from django.views.generic import TemplateView

from django.contrib import admin

from accounts import urls as accounts_url
from api.routers import router as api_router
from cms import urls as cms_url
import equipments.urls as equipments_urls
import publishing.urls as publishing_urls

from equipments.views.activity import ActivityListView

from cms.views import AboutView, PrivacyPolicyView, LegalNoticeView

urlpatterns = [
    path('about-us/', AboutView.as_view(), name='about-us'),
    path('privacy-policy', PrivacyPolicyView.as_view(), name='privacy-policy'),
    path('legal-notice', LegalNoticeView.as_view(), name='legal-notice'),

    path('admin/', admin.site.urls),

    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),

    # path('', TemplateView.as_view(template_name='site/index.html'), name='home'),
    path('', ActivityListView.as_view(template_name='site/home.html'), name='home'),
    re_path(r'^(?P<page>\d+)$', ActivityListView.as_view(template_name='site/home.html'), name='home'),
    path('activities/', ActivityListView.as_view(), name='activities'),
    re_path(r'^activities/(?P<page>\d+)$', ActivityListView.as_view(), name='activities'),
    path('coming-soon/', TemplateView.as_view(template_name='site/coming-soon.html'), name='coming-soon'),

    url('', include(cms_url, namespace='cms')),
    url('', include(equipments_urls, namespace='equipments')),
    url('accounts/', include(accounts_url, namespace='accounts')),
    url('api/', include((api_router.urls, 'api'), namespace='api')),
    url('publishing/', include(publishing_urls, namespace='publishing')),

    url('accounts/', include('allauth.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
