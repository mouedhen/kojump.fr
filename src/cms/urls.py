from django.urls import path

from cms.views import AboutView, LegalNoticeView, PrivacyPolicyView

app_name = 'cms'

urlpatterns = [
    path('about-us/', AboutView.as_view(), name='about-us'),
    path('legal-notice/', LegalNoticeView.as_view(), name='legal-notice'),
    path('privacy-policy/', PrivacyPolicyView.as_view(), name='privacy-policy'),
]
