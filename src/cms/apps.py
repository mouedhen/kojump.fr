from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class CmsConfig(AppConfig):
    name = 'cms'
    verbose_name = _('Gestion du contenu')
