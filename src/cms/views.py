from django.shortcuts import redirect, reverse

from django.views.generic.base import TemplateView

from cms.models.cms import About, LegalNotice, PrivacyPolicy, Team


class AboutView(TemplateView):

    template_name = 'site/about.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = About.published_objects.first()
        context['teams'] = Team.published_objects.all()
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        # if not context['object']:
        #     return redirect(reverse('coming-soon'))
        return self.render_to_response(context)


class LegalNoticeView(TemplateView):

    template_name = 'legal/copyright-policy.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = LegalNotice.published_objects.first()
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        # if not context['object']:
        #     return redirect(reverse('coming-soon'))
        return self.render_to_response(context)


class PrivacyPolicyView(TemplateView):

    template_name = 'legal/copyright-policy.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = PrivacyPolicy.published_objects.first()
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        # if not context['object']:
        #     return redirect(reverse('coming-soon'))
        return self.render_to_response(context)
