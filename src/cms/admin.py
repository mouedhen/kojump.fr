from django.contrib import admin

from .models.cms import About, LegalNotice, PrivacyPolicy, Slide, Team


@admin.register(About)
class AboutAdmin(admin.ModelAdmin):
    save_on_top = True

    list_display = ('title', 'status',)
    list_filter = ('status', 'created',)

    prepopulated_fields = {'slug': ('title',), }
    readonly_fields = ('created', 'modified',)


@admin.register(LegalNotice)
class LegalNoticeAdmin(admin.ModelAdmin):
    save_on_top = True

    list_display = ('title', 'status',)
    list_filter = ('status', 'created',)

    prepopulated_fields = {'slug': ('title',), }
    readonly_fields = ('created', 'modified',)


@admin.register(PrivacyPolicy)
class LegalNoticeAdmin(admin.ModelAdmin):
    save_on_top = True

    list_display = ('title', 'status',)
    list_filter = ('status', 'created',)

    prepopulated_fields = {'slug': ('title',), }
    readonly_fields = ('created', 'modified',)


@admin.register(Slide)
class SlideAdmin(admin.ModelAdmin):
    save_on_top = True

    list_display = ('image_tag', 'text',)
    list_filter = ('created',)

    readonly_fields = ('image_tag', 'created', 'modified',)


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    save_on_top = True

    list_display = ('title', 'image_tag', 'content_tag', 'status',)
    list_filter = ('status', 'created',)

    prepopulated_fields = {'slug': ('title',), }
    readonly_fields = ('image_tag', 'created', 'modified',)
