from django.db import models
from django.db.models.signals import post_delete, pre_save
from django.utils.translation import ugettext_lazy as _

from stdimage.models import StdImageField
from stdimage.utils import pre_delete_delete_callback, pre_save_delete_callback, UploadToClassNameDirUUID

from cms.models.abstracts import TimeStampedAbstractModel
from cms.models.base import Card, Page
from cms.models.utils import archive_published_callback


class About(Page):
    class Meta:
        verbose_name = _('page - à propos')
        verbose_name_plural = _('pages - à propos')


pre_save.connect(archive_published_callback, sender=About)


class LegalNotice(Page):
    class Meta:
        verbose_name = _('page - mentions légales')
        verbose_name_plural = _('pages - mentions légales')


pre_save.connect(archive_published_callback, sender=LegalNotice)


class PrivacyPolicy(Page):
    class Meta:
        verbose_name = _('page - confidentialité & cookies')
        verbose_name_plural = _('pages - confidentialité & cookies')


pre_save.connect(archive_published_callback, sender=PrivacyPolicy)


class Slide(TimeStampedAbstractModel):
    class Meta:
        verbose_name = _('slide de présentation')
        verbose_name_plural = _('slides de présentation')

    text = models.CharField(_('texte'), max_length=254, blank=True)
    image = StdImageField(
        upload_to=UploadToClassNameDirUUID(),
        blank=True,
        variations={
            'large': (1426, 744, True),
            'thumbnail': (570, 297, True),
        })

    def image_tag(self):
        from django.utils.safestring import mark_safe
        return mark_safe('<img src="%s" width="570" height="297" />' % self.image.thumbnail.url)

    image_tag.short_description = _('slide (aperçu)')


class Team(Card):
    class Meta:
        verbose_name = _('membre équipe')
        verbose_name_plural = _('membres équipe')


post_delete.connect(pre_delete_delete_callback, sender=Team)
pre_save.connect(pre_save_delete_callback, sender=Team)
