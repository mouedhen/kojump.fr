from django.db.models.signals import post_delete, pre_save
from django.utils.translation import ugettext_lazy as _

from stdimage.models import StdImageField
from stdimage.utils import pre_delete_delete_callback, pre_save_delete_callback, UploadToClassNameDirUUID

from cms.models.abstracts import PageComponent


class Page(PageComponent):
    class Meta:
        verbose_name = _('page')
        verbose_name_plural = _('pages')


class Component(PageComponent):
    class Meta:
        verbose_name = _('composant')
        verbose_name_plural = _('composants')


class Card(PageComponent):
    class Meta:
        verbose_name = _('card')
        verbose_name_plural = _('cards')

    card_image = StdImageField(
        upload_to=UploadToClassNameDirUUID(),
        blank=True,
        variations={
            'thumbnail': (300, 400, True),
        })

    def image_tag(self):
        from django.utils.safestring import mark_safe
        return mark_safe('<img src="%s" width="300" height="400" />' % self.card_image.thumbnail.url)

    image_tag.short_description = _('card image (aperçu)')


post_delete.connect(pre_delete_delete_callback, sender=Card)
pre_save.connect(pre_save_delete_callback, sender=Card)
