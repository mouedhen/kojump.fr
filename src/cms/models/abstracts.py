from django.db import models
from django.utils.translation import ugettext_lazy as _

from ckeditor_uploader.fields import RichTextUploadingField

from core.models.abstracts import TimeStampedAbstractModel


class PublishedManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status__exact='PUBLISHED')


class PageComponent(TimeStampedAbstractModel):

    STATUS_CHOICE = (
        ('DRAFT', _('Brouillon')),
        ('PUBLISHED', _('Publié(e)')),
        ('ARCHIVED', _('Archivé(e)')),
    )

    class Meta:
        abstract = True

    title = models.CharField(_('titre'), max_length=150)
    slug = models.SlugField(_('slug'), max_length=150)
    content = RichTextUploadingField(_('contenu'), blank=True)
    status = models.CharField(_(u'statut'), max_length=9, choices=STATUS_CHOICE, default='DRAFT')

    def __str__(self):
        return self.title

    def content_tag(self):
        from django.utils.safestring import mark_safe
        return mark_safe(self.content)

    content_tag.short_description = _('contenu (aperçu)')

    objects = models.Manager()  # The default manager.
    published_objects = PublishedManager()  # The Dahl-specific manager.
