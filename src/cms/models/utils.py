def archive_published_callback(sender, instance, **kwargs):
    if instance.status == 'PUBLISHED':
        instance.__class__.published_objects.all().update(status='ARCHIVED')
