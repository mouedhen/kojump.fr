import logging

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

logger = logging.getLogger(__name__)
DEFAULT_ADMIN_PASSWORD = 'uGv2Ww74'


class Command(BaseCommand):

    help = 'Command to create a default super user.'

    def handle(self, *args, **options):
        user = User(username='admin', email='webmaster.kojump@gmail.com',
                    is_superuser=True, is_staff=True, is_active=True)
        user.set_password(DEFAULT_ADMIN_PASSWORD)
        user.save()
        # user.refresh_from_db()
        # profile = Profile(is_active=True, user=user, country=u'France',)
        # profile.save()
        logger.info('[SUCCESS][SEEDER] User created - username: {}'.format(user.username))
