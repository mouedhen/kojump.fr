(function () {

    var institution_id = document.getElementById('institutionId').value;

    L.mapbox.accessToken = 'pk.eyJ1IjoibW91ZWRoZW4iLCJhIjoiY2o1b25ibHdvMDFrbTJxcXRtaWttZ3VqcCJ9.D5l-yqWthlUroyn5GFoY2w';

    var map = L.mapbox.map('map', 'mapbox.streets')
        .setView([48.856638, 2.352241], 12);

    var featureLayer = L.mapbox.featureLayer()
        .loadURL('/api/equipments/?institution=' + institution_id)
        .addTo(map);

    featureLayer.on('ready', function (e) {
        var i = 0;
        this.eachLayer(function (marker) {
            marker.setIcon(L.mapbox.marker.icon({
                'marker-size': 'small',
                'marker-symbol': 'star',
                'marker-color': (new FlatColor()).hex
            }));
            if (i === 0) {
                map.setView([marker.feature.geometry.coordinates[1], marker.feature.geometry.coordinates[0]], 14)
            }
            marker.bindPopup('<div><h1><a href="/equipments/' + marker.feature.id + "/" + marker.feature.properties.slug +'">' +
                marker.feature.properties.label + '</a></h1>\n' +
                '</div>\n');
        });
    });
})();