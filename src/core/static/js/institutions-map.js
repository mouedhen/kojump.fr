function defineElChecked(elementId) {
    if (document.getElementById(elementId) !== null)
        document.getElementById(elementId).checked = true
}

function buildUrl() {
    var urlBuilder = new UrlBuilder('institutions');

    if (document.getElementById('activityId') !== null) {
        var params = {activityInputId: 'activityId', isPublicInputId: 'is_public'}
    } else {
        var params = {isPublicInputId: 'is_public'}
    }
    urlBuilder.buildUrlFromGetParams(params);

    document.getElementById('lng-search').value = urlBuilder.lng;
    document.getElementById('lat-search').value = urlBuilder.lat;
    document.getElementById('distance-search').value = urlBuilder.distance / 1000;

    if (urlBuilder.disciplines instanceof Array) {
        urlBuilder.disciplines.forEach(function (discipline) {
            defineElChecked('discipline_' + discipline)
        })
    } else {
        if (urlBuilder.disciplines !== undefined) {
            defineElChecked('discipline_' + urlBuilder.disciplines)
        }
    }

    if (urlBuilder.activities instanceof Array) {
        urlBuilder.activities.forEach(function (activity) {
            defineElChecked('activity_' + activity)
        })
    } else {
        if (urlBuilder.activities !== undefined) {
            defineElChecked('activity_' + urlBuilder.activities)
        }
    }

    console.log(urlBuilder.url)
    return urlBuilder
}

$(document).ready(function () {

    var urlBuilder = buildUrl();

    L.mapbox.accessToken = 'pk.eyJ1IjoibW91ZWRoZW4iLCJhIjoiY2o1b25ibHdvMDFrbTJxcXRtaWttZ3VqcCJ9.D5l-yqWthlUroyn5GFoY2w';

    var map = L.mapbox.map('map', 'mapbox.streets')
        .setView([urlBuilder.lat, urlBuilder.lng], 13);

    var map2 = L.mapbox.map('map2', 'mapbox.streets')
        .setView([urlBuilder.lat, urlBuilder.lng], 13);

    var lc = L.control.locate({keepCurrentZoomLevel: true, flyTo: true}).addTo(map);
    var lc2 = L.control.locate({keepCurrentZoomLevel: true, flyTo: true}).addTo(map2);

    map.addControl(L.mapbox.geocoderControl('mapbox.places', {autocomplete: true}));
    map2.addControl(L.mapbox.geocoderControl('mapbox.places', {autocomplete: true}));

    var featureLayer = L.mapbox.featureLayer().loadURL(urlBuilder.url).addTo(map);
    var featureLayer2 = L.mapbox.featureLayer().loadURL(urlBuilder.url).addTo(map2);

    var circle = L.circle(new L.LatLng(urlBuilder.lat, urlBuilder.lng), urlBuilder.distance).addTo(map);
    var circle2 = L.circle(new L.LatLng(urlBuilder.lat, urlBuilder.lng), urlBuilder.distance).addTo(map2);

    var marker = L.marker(new L.LatLng(urlBuilder.lat, urlBuilder.lng), {
        icon: L.mapbox.marker.icon({
            'marker-color': '#3bb2d0',
            'marker-size': 'large',
            'marker-symbol': 'embassy'
        }),
        draggable: true
    }).addTo(map);

    var marker2 = L.marker(new L.LatLng(urlBuilder.lat, urlBuilder.lng), {
        icon: L.mapbox.marker.icon({
            'marker-color': '#3bb2d0',
            'marker-size': 'large',
            'marker-symbol': 'embassy'
        }),
        draggable: true
    }).addTo(map2);

    window.addEventListener('load', function () {
        var searchPin = document.querySelector('body > section > article > form > div.uk-margin-bottom.uk-width-1-1 > span > button.ap-input-icon.ap-icon-pin > svg');

        if (searchPin !== null) {
            searchPin.addEventListener('click', (evt) => {
                lc.start();
                lc2.start()
            });
        }
    });

    map.on('locationfound', function (e) {
        marker.setLatLng(e.latlng);
        circle.setLatLng(e.latlng).setRadius(urlBuilder.distance);
        urlBuilder.lng = e.latlng.lng;
        urlBuilder.lat = e.latlng.lat;
        document.getElementById('lng-search').value = urlBuilder.lng;
        document.getElementById('lat-search').value = urlBuilder.lat;
        urlBuilder.buildUrl();
        featureLayer.loadURL(urlBuilder.url);
        lc.stop()
    });

    map2.on('locationfound', function (e) {
        marker2.setLatLng(e.latlng);
        circle2.setLatLng(e.latlng).setRadius(urlBuilder.distance);
        urlBuilder.lng = e.latlng.lng;
        urlBuilder.lat = e.latlng.lat;
        document.getElementById('lng-search').value = urlBuilder.lng;
        document.getElementById('lat-search').value = urlBuilder.lat;
        urlBuilder.buildUrl();
        featureLayer2.loadURL(urlBuilder.url);
        lc2.stop()
    });

    featureLayer.on('ready', function (e) {
        this.eachLayer(function (marker) {
            marker.setIcon(L.mapbox.marker.icon({
                'marker-size': 'small',
                'marker-symbol': 'star',
                'marker-color': '#E65100'
            }));
            marker.bindPopup('<div><a href="/institutions/' + marker.feature.id + '/' + marker.feature.properties.slug + '/">' +
                marker.feature.properties.label + '</a>\n' + '<p>' + marker.feature.properties.address + '</p>\n' +
                '</div>\n');
        });
        onmove();
        document.getElementById('progress-bar').style.display = 'none';
    });

    featureLayer2.on('ready', function (e) {
        this.eachLayer(function (marker) {
            marker.setIcon(L.mapbox.marker.icon({
                'marker-size': 'small',
                'marker-symbol': 'star',
                'marker-color': '#E65100'
            }));
            marker.bindPopup('<div><a href="/institutions/' + marker.feature.id + '/' + marker.feature.properties.slug + '/">' +
                marker.feature.properties.label + '</a>\n' + '<p>' + marker.feature.properties.address + '</p>\n' +
                '</div>\n');
        });
        onmove2();
        document.getElementById('progress-bar').style.display = 'none';
    });

    marker.on('dragstart', function () {
        document.getElementById('progress-bar').style.display = 'block';
        circle.setRadius(1);
    });

    marker2.on('dragstart', function () {
        document.getElementById('progress-bar').style.display = 'block';
        circle2.setRadius(1);
    });

    marker.on('dragend', function (e) {
        circle.setLatLng(e.target.getLatLng()).setRadius(urlBuilder.distance);
        urlBuilder.lng = e.target.getLatLng().lng;
        urlBuilder.lat = e.target.getLatLng().lat;
        document.getElementById('lng-search').value = urlBuilder.lng;
        document.getElementById('lat-search').value = urlBuilder.lat;
        urlBuilder.buildUrl();
        featureLayer.loadURL(urlBuilder.url);
    });

    marker2.on('dragend', function (e) {
        circle2.setLatLng(e.target.getLatLng()).setRadius(urlBuilder.distance);
        urlBuilder.lng = e.target.getLatLng().lng;
        urlBuilder.lat = e.target.getLatLng().lat;
        document.getElementById('lng-search').value = urlBuilder.lng;
        document.getElementById('lat-search').value = urlBuilder.lat;
        urlBuilder.buildUrl();
        featureLayer2.loadURL(urlBuilder.url);
    });

    var $slider = $('.range-slider');
    var $range = $('.range-slider__range');

    $slider.each(function () {

        $('.range-slider__value').each(function () {
            $(this).html(urlBuilder.distance / 1000);
        });

        $range.on('input', function () {
            document.getElementById('progress-bar').style.display = 'block';
            urlBuilder.distance = this.value * 1000;
            circle.setRadius(urlBuilder.distance);
            circle2.setRadius(urlBuilder.distance);
            urlBuilder.buildUrl();
            featureLayer.loadURL(urlBuilder.url)
            featureLayer2.loadURL(urlBuilder.url)
        });
    });

    map.on('moveend', onmove);
    map2.on('moveend', onmove2);

    var renderFeature = function (feature) {
        var item = document.createElement('div');
        item.classList.add('activity');
        item.innerHTML = '<div class="uk-card uk-card-default uk-card-hover">\n' +
            '               <div class="uk-card-media-top">\n' +
            '                   <a href="/institutions/' + feature.id + '/' + feature.properties.slug + '/"><img src="' + feature.properties.image + '" alt="' + feature.properties.slug + '"></a>\n' +
            '               </div>\n' +
            '               <div class="uk-card-body">\n' +
            '                   <h3 class="uk-card-title">' +
            '                     <a href="/institutions/' + feature.id + '/' + feature.properties.slug + '/">' + feature.properties.label + '</a>' +
            '                   </h3>\n' +
            '                   <p>' + feature.properties.address + '</p>\n' +
            '               </div>\n' +
            '           </div>';
        return item;
    };

    function onmove2() {
        var featuresContainer = document.getElementById('map-elements2')
        featuresContainer.innerHTML = '';
        if (featureLayer2.getGeoJSON().features.length <= 0) {
            featuresContainer.innerHTML = 'Aucun élément ne correspond à vos critères de recherche';
        }
        featureLayer2.eachLayer(function (marker) {
            featuresContainer.appendChild(renderFeature(marker.feature))
        });
    }

    function onmove() {
        var bounds = map.getBounds();
        document.getElementById('map-elements').innerHTML = '';
        featureLayer.eachLayer(function (marker) {
            if (bounds.contains(marker.getLatLng())) {
                document.getElementById('map-elements').appendChild(renderFeature(marker.feature))
            }
        });
    }
});
