import UIkit from 'uikit'
import Icons from 'uikit/dist/js/uikit-icons'
import axios from 'axios'
import places from 'places.js'

import FlatColor from './widgets/flatcolor';
import UrlBuilder from './widgets/url-builder';
import MapControl from './map-control'
import {RatingStars} from "./widgets/RatingStars";


require('./hero-slider');
require('./sticky-map');
require('./equipments-rating');


window.axios = axios;
window.FlatColor = FlatColor;
window.UrlBuilder = UrlBuilder;

UIkit.use(Icons);

function generateUrl(url, params) {
    let i = 0, key;
    for (key in params) {
        if (i === 0) {
            url += "?";
        } else {
            url += "&";
        }
        url += key;
        url += '=';
        url += params[key];
        i++;
    }
    return url;
}

window.addEventListener('load', function () {

    let placesAutoComplete = places({
        container: document.querySelector('#address-input'),
        language: 'fr',
        countries: ['fr'],
    });

    let placesAutoComplete2 = places({
        container: document.querySelector('#address-input2'),
        language: 'fr',
        countries: ['fr'],
    });

    // @TODO send data to the server
    placesAutoComplete.on('change', function (e) {
        let url = generateUrl(location.origin + '/search/', {
            postalcode: e.suggestion.hit.postcode[0],
            lat: e.suggestion.hit._geoloc.lat,
            lng: e.suggestion.hit._geoloc.lng,
            place: e.suggestion.value,
        });

        window.location.replace(url)
    });

    // @TODO send data to the server
    placesAutoComplete2.on('change', function (e) {
        let url = generateUrl(location.origin + '/search/', {
            postalcode: e.suggestion.hit.postcode[0],
            lat: e.suggestion.hit._geoloc.lat,
            lng: e.suggestion.hit._geoloc.lng,
            place: e.suggestion.value,
        });

        window.location.replace(url)
    });

    let searchBarPin = document.querySelector('body > div.uk-sticky.uk-sticky-fixed > nav > div.uk-navbar-right.uk-visible\\40 s > ul > li:nth-child(1) > span > button.ap-input-icon.ap-icon-pin > svg');
    if (searchBarPin !== null) {
        searchBarPin.addEventListener('click', (evt) => {
            navigator.geolocation.getCurrentPosition(position => {
                let url = generateUrl(location.origin + '/search/', {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                });
                window.location.replace(url)
            })
        });
    }

    let searchBarPin2 = document.querySelector('#offcanvas-nav > div > ul > li.uk-navbar-item.off-form > span > button.ap-input-icon.ap-icon-pin > svg');
    if (searchBarPin2 !== null) {
        searchBarPin2.addEventListener('click', (evt) => {
            navigator.geolocation.getCurrentPosition(position => {
                let url = generateUrl(location.origin + '/search/', {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                });
                window.location.replace(url)
            })
        });
    }

    let element = document.getElementById('address-search');
    if (typeof(element) !== 'undefined' && element !== null) {
        global.placesSearchAutoComplete = places({
            container: element,
            language: 'fr',
            countries: ['fr'],
        });
        placesSearchAutoComplete.on('change', function (e) {
            console.log(e.suggestion);
            document.getElementById('lng-search').value = e.suggestion.hit._geoloc.lng;
            document.getElementById('lat-search').value = e.suggestion.hit._geoloc.lat;
        });
    }

    if (document.getElementById('map-control') !== null) {
        let control = new MapControl(
            document.getElementById('map-control'),
            document.getElementById('map-elements-container'),
            document.getElementById('map-elements'),
            document.getElementById('map-container'),
            document.getElementById('toggle-elements'),
            document.getElementById('toggle-map')
        );
        control.render('map-elements', 'bottom-scroll-spy');
    }

    if (document.getElementById('global-rating')) {
        console.log(document.getElementById('global-rating').dataset.rate)
        // new RatingStars({
        //     containerEl: document.querySelector('.global-rating'),
        //     starEl: document.querySelector('.global-star-rating'),
        //     starsEl: document.getElementsByClassName('global-star-rating'),
        //     readOnly: true,
        //     weight: document.getElementById('global-rating').dataset.rate,
        // });
    }
});
