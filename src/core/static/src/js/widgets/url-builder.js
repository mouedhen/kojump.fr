import getAllUrlParams from './url-params'

class UrlBuilder {
    constructor(apiEndPoint) {
        this.activities = undefined;
        this.disciplines = undefined;
        this.is_public = undefined;
        this.lat = 48.864716;
        this.lng = 2.349014;
        this.distance = 3000;

        this._baseUrl = '//' + window.location.host + '/api/' + apiEndPoint + '/?';
        this.url = this._baseUrl;
        this.urlParams = getAllUrlParams();
    }

    buildUrlFromGetParams(options = {activityInputId: null, disciplineInputId: null, isPublicInputId: null}) {

        this.setLatFromGetParams();
        this.setLngFromGetParams();
        this.setDistanceFromGetParams();
        this.setActivities(options.activityInputId);
        this.setDisciplines(options.disciplineInputId);
        this.setPublicPrivate(options.isPublicInputId);

        this.buildUrl()
    }

    buildUrl() {
        this.url = this._baseUrl + 'point=' + this.lng + ',' + this.lat + '&dist=' + this.distance +
            this.generateActivitiesUrlParams() + this.generateDisciplinesUrlParams() + this.generatePublicPrivate()
    }

    setLatFromGetParams() {
        if (this.urlParams.lat !== undefined) {
            this.lat = this.urlParams.lat
        }
    }

    setLngFromGetParams() {
        if (this.urlParams.lng !== undefined) {
            this.lng = this.urlParams.lng
        }
    }

    setDistanceFromGetParams() {
        if (this.urlParams.distance !== undefined) {
            this.distance = this.urlParams.distance * 1000
        }
    }

    setActivities(inputId = null) {
        if (inputId !== null) {
            this.activities = document.getElementById(inputId).value;
            return 0;
        }
        this.activities = this.urlParams.activities;
        return 2
    }

    setPublicPrivate() {
        // if (this.urlParams.is_public === 'False' && document.getElementById('is_private')) {
        if (this.urlParams.is_public === 'False') {
            document.getElementById('is_private').checked = true
        // } else if (this.urlParams.is_public === 'True' && document.getElementById('is_public')) {
        } else if (this.urlParams.is_public === 'True') {
            document.getElementById('is_public').checked = true
        } else if (document.getElementById('not_defined')) {
            document.getElementById('not_defined').checked = true
        }
        this.is_public = this.urlParams.is_public;
        return 0
    }

    generatePublicPrivate() {
        // if (this.is_public instanceof Array) {
        //     let result = '';
        //     this.is_public.forEach(function (is_public) {
        //         result += '&is_public=' + is_public;
        //     });
        //     return result
        // }
        if (this.is_public !== undefined) {
            return '&is_public=' + this.is_public;
        }
        return '';
    }

    generateActivitiesUrlParams() {
        if (this.activities instanceof Array) {
            let result = '';
            this.activities.forEach(function (activity) {
                result += '&activities=' + activity;
            });
            return result
        }
        if (this.activities !== undefined) {
            return '&activity=' + this.activities;
        }
        return '';
    }

    setDisciplines(inputId = null) {
        if (inputId !== null) {
            this.disciplines = document.getElementById(inputId).value;
            return 0;
        }
        this.disciplines = this.urlParams.disciplines;
        return 2
    }

    generateDisciplinesUrlParams() {
        if (this.disciplines instanceof Array) {
            let result = '';
            this.disciplines.forEach(function (discipline) {
                result += '&disciplines=' + discipline;
            });
            return result
        }
        if (this.disciplines !== undefined) {
            return '&discipline=' + this.disciplines;
        }
        return '';
    }

}

export default UrlBuilder;
