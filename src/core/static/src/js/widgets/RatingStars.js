import Rating from 'rating'

export class RatingStars {

    constructor(options = {
        containerEl: null,
        starEl: null,
        starsEl: null,
        target: null,
        readOnly: false,
        weight: 0
    }, callback) {
        if (options.containerEl !== null && options.starEl !== null && options.starsEl !== null) {
            this.stars = options.starsEl;
            this.target = document.getElementById(options.target);

            options.starEl.parentNode.removeChild(options.starEl);
            this.rating = new Rating([1, 2, 3, 4, 5], {
                container: options.containerEl,
                star: options.starEl,
                readOnly: options.readOnly
            });

            let that = this;

            this.rating.on('rate', function (weight) {
                if (weight !== undefined) {
                    that.rateHandler(weight);
                    if (that.target !== null) {
                        that.target.value = weight
                    }
                }
            });
            that.rating.ratingIdx = options.weight
            // this.rating.on('select', function (weight) {
            //     if (weight !== undefined) {
            //         that.rateHandler(weight);
            //         console.log('target weight', that.rateTarget, weight);
            //     }
            // });
        }
    }

    rateHandler(weight) {
        for (let i = 0; i < this.stars.length; i++) {
            if (i < weight) {
                this.stars[i].firstChild.firstChild.nextElementSibling.setAttribute('fill', '#ffeb3b !important')
            } else {
                this.stars[i].firstChild.firstChild.nextElementSibling.setAttribute('fill', 'none')
            }
        }
    }
}

// export default RatingStars;