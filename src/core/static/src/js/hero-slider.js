import TextRotate from "./widgets/text-rotate";

if (document.getElementById('hero') !== null) {
    let elements = document.getElementsByClassName('txt-rotate');

    for (let i = 0; i < elements.length; i++) {

        let toRotate = elements[i].getAttribute('data-rotate');
        let period = elements[i].getAttribute('data-period');
        let heroSlider = document.getElementById('hero');

        if (toRotate) {
            let textRotate = new TextRotate(elements[i], JSON.parse(toRotate), period);

            heroSlider.addEventListener('beforeitemshow', (e) => {
                textRotate.isDeleting = true;
                textRotate.txt = '';
            });
            heroSlider.addEventListener('itemhide', (e) => {
                textRotate.loopNum++;
            });
        }
    }
}
