import {RatingStars} from './widgets/RatingStars'

new RatingStars({
    containerEl: document.querySelector('.global-rate'),
    starEl: document.querySelector('.global-star'),
    starsEl: document.getElementsByClassName('global-star'),
    target: 'global-rate',
});

new RatingStars({
    containerEl: document.querySelector('.hygiene-rate'),
    starEl: document.querySelector('.hygiene-star'),
    starsEl: document.getElementsByClassName('hygiene-star'),
    target: 'hygiene-rate',
});

new RatingStars({
    containerEl: document.querySelector('.services-rate'),
    starEl: document.querySelector('.services-star'),
    starsEl: document.getElementsByClassName('services-star'),
    target: 'services-rate',
});

new RatingStars({
    containerEl: document.querySelector('.equipments-rate'),
    starEl: document.querySelector('.equipments-star'),
    starsEl: document.getElementsByClassName('equipments-star'),
    target: 'equipments-rate',
});
