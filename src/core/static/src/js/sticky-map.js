import Sticky from 'sticky-js';

if (document.getElementById('map') !== null &&
    document.getElementsByClassName('sticky-map')[0] !== undefined) {
    let sticky = new Sticky('[data-sticky]');
    document.getElementById('toggle-map').addEventListener('click', function (e) {
        e.preventDefault();
        sticky.update();
    })
}

if (document.getElementById('sticky-nav') !== null) {
    let sticky = new Sticky('[data-sticky]');
    sticky.update();
}
