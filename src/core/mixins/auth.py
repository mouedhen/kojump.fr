from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponseRedirect
from django.shortcuts import resolve_url


def _is_authenticated(user):
    return user.is_authenticated


class AnonymousRequiredMixin(object):

    authenticated_redirect_url = '/accounts/profile/'

    def dispatch(self, request, *args, **kwargs):
        if _is_authenticated(request.user):
            return HttpResponseRedirect(self.get_authenticated_redirect_url())
        return super(AnonymousRequiredMixin, self).dispatch(request, *args, **kwargs)

    def get_authenticated_redirect_url(self):
        if not self.authenticated_redirect_url:
            raise ImproperlyConfigured(
                '{0} is missing an authenticated_redirect_url url to redirect to. Define '
                '{0}.authenticated_redirect_url or override '
                '{0}.get_authenticated_redirect_url().'.format(
                    self.__class__.__name__))
        return resolve_url(self.authenticated_redirect_url)

    def set_authenticated_redirect_url(self, url):
        self.authenticated_redirect_url = url

