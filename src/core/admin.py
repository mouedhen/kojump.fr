from django.contrib import admin

from .models.attachement import AttachedMedia

# Register your models here.
admin.site.register(AttachedMedia)
