from slugify import slugify

from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey


class ActiveManager(models.Manager):
    def get_queryset(self):
        return super(ActiveManager, self).get_queryset().filter(is_active=True)


class ActivatedAbstractModel(models.Model):
    class Meta:
        abstract = True

    is_active = models.BooleanField(_(u'est actif/active ?'), default=True)

    objects = models.Manager()
    active_objects = ActiveManager()


class DescribedAbstractModel(models.Model):
    description = models.TextField(_('description'), blank=True, null=True)

    class Meta:
        abstract = True


class GenericRelationAbstractModel(models.Model):
    class Meta:
        abstract = True

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()


class LocatedAbstractModel(models.Model):
    class Meta:
        abstract = True

    street_address = models.CharField(_('adresse postale'), max_length=254, blank=True)
    street_address2 = models.CharField(_('adresse postale (complément)'), max_length=254, blank=True)
    postal_code = models.CharField(_('code postal'), max_length=10, blank=True)
    city = models.CharField(_('ville'), max_length=100, blank=True)
    country = models.CharField(_('pays'), max_length=100, blank=True)


class SluggedAbstractModel(models.Model):
    class Meta:
        abstract = True

    label = models.CharField(_('label'), max_length=254)
    slug = models.SlugField(_('slug'), max_length=254)

    def generate_slug(self):
        self.slug = slugify(self.label)


class TimeStampedAbstractModel(models.Model):
    class Meta:
        abstract = True

    created = models.DateTimeField(_('date de création'), auto_now_add=True)
    modified = models.DateTimeField(_('date de modification'), auto_now=True)
