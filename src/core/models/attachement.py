from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey


class AttachedMedia(models.Model):

    class Meta:
        verbose_name = _('media associé')
        verbose_name_plural = _('medias associés')

    image = models.ImageField(_('image'), upload_to='attached_media')

    # Generic relation to allow dynamic relation definition with comment
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()
