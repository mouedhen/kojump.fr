from django.contrib import admin

from django.contrib.contenttypes.admin import GenericTabularInline
from django.utils.translation import ugettext_lazy as _

from import_export.admin import ImportExportModelAdmin


from core.models.attachement import AttachedMedia
from mapwidgets.widgets import GooglePointFieldWidget
from django.contrib.gis.db import models

from .models.categories import (
    CategoryFamily,
    Category,
    Activity,
    SportDiscipline,
    ActivityLevel,
    SiteEnvironment,
    SiteGround,
    UsageType,
    EquipmentTarget,
    ManagerCategory,
    SpecialInstitution,
)
from .models.locations import Department, Commune
from .models.institution import Institution
from .models.equipment import Equipment, EquipmentSportDiscipline

# Register your models here.
# admin.site.register(CategoryFamily)
# admin.site.register(Category)
# admin.site.register(Activity)
# admin.site.register(SportDiscipline)
# admin.site.register(ActivityLevel)
# admin.site.register(SiteEnvironment)
# admin.site.register(SiteGround)
# admin.site.register(UsageType)
# admin.site.register(EquipmentTarget)
# admin.site.register(ManagerCategory)
# admin.site.register(SpecialInstitution)
#
# admin.site.register(Department)
# admin.site.register(Commune)
#
# admin.site.register(Institution)
# admin.site.register(Equipment)
#
# admin.site.register(EquipmentSportDiscipline)


class EquipmentCategoryInline(admin.TabularInline):

    model = Category
    prepopulated_fields = {'slug': ('label',), }


@admin.register(CategoryFamily)
class EquipmentCategoryFamilyAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    save_on_top = True

    list_display = ('label', 'created', 'is_active')
    list_filter = ('is_active', 'created',)
    ordering = ('label',)

    search_fields = ('label',)

    readonly_fields = ('created', 'modified')
    prepopulated_fields = {'slug': ('label',), }

    inlines = [
        EquipmentCategoryInline
    ]


@admin.register(Category)
class EquipmentCategoryAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    save_on_top = True

    list_display = ('label', 'family', 'created', 'is_active')
    list_filter = ('is_active', 'created', 'family')
    ordering = ('label',)

    search_fields = ('label',)

    readonly_fields = ('created', 'modified')
    prepopulated_fields = {'slug': ('label',), }


@admin.register(ActivityLevel)
class ActivityLevelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    save_on_top = True

    list_display = ('label', 'created', 'is_active',)
    list_filter = ('is_active', 'created',)
    ordering = ('label',)

    search_fields = ('label',)

    readonly_fields = ('created', 'modified')
    prepopulated_fields = {'slug': ('label',), }


@admin.register(SiteEnvironment)
class SiteEnvironmentAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    save_on_top = True

    list_display = ('label', 'created', 'is_active',)
    list_filter = ('is_active', 'created',)
    ordering = ('label',)

    search_fields = ('label',)

    readonly_fields = ('created', 'modified')
    prepopulated_fields = {'slug': ('label',), }


@admin.register(SiteGround)
class SiteGroundAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    save_on_top = True

    list_display = ('label', 'created', 'is_active',)
    list_filter = ('is_active', 'created',)
    ordering = ('label',)

    search_fields = ('label',)

    readonly_fields = ('created', 'modified')
    prepopulated_fields = {'slug': ('label',), }


@admin.register(UsageType)
class UsageTypeAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    save_on_top = True

    list_display = ('label', 'created', 'is_active',)
    list_filter = ('is_active', 'created',)
    ordering = ('label',)

    search_fields = ('label',)

    readonly_fields = ('created', 'modified')
    prepopulated_fields = {'slug': ('label',), }


@admin.register(EquipmentTarget)
class EquipmentTargetAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    save_on_top = True

    list_display = ('label', 'created', 'is_active',)
    list_filter = ('is_active', 'created',)
    ordering = ('label',)

    search_fields = ('label',)

    readonly_fields = ('created', 'modified')
    prepopulated_fields = {'slug': ('label',), }


@admin.register(ManagerCategory)
class ManagerCategoryAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    save_on_top = True

    list_display = ('label', 'is_public', 'created', 'is_active',)
    list_filter = ('is_active', 'is_public', 'created',)
    ordering = ('label',)

    search_fields = ('label',)

    readonly_fields = ('created', 'modified')
    prepopulated_fields = {'slug': ('label',), }


class InlineInstitution(admin.StackedInline):

    model = Institution
    extra = 0
    can_delete = False
    fields = ('code', 'label', 'commune')
    readonly_fields = ('code', 'label', 'commune')


@admin.register(SpecialInstitution)
class SpecialInstitutionAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    save_on_top = True

    list_display = ('label', 'created', 'is_active',)
    list_filter = ('is_active', 'created',)
    ordering = ('label',)

    search_fields = ('label',)

    readonly_fields = ('created', 'modified')
    prepopulated_fields = {'slug': ('label',), }


class CommuneInline(admin.TabularInline):

    model = Commune
    extra = 0
    can_delete = False

    fields = ('code_insee', 'label', 'is_active')
    readonly_fields = ('code_insee', 'label',)

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Department)
class DepartmentAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    save_on_top = True

    list_display = ('label', 'code', 'created', 'is_active',)
    list_filter = ('is_active', 'created',)
    ordering = ('label',)

    search_fields = ('label', 'code')

    readonly_fields = ('created', 'modified')
    prepopulated_fields = {'slug': ('label',), }

    inlines = [
        CommuneInline
    ]


@admin.register(Commune)
class CommuneAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    save_on_top = True

    list_display = ('label', 'code_insee', 'created', 'is_active',)
    list_filter = ('is_active', 'created',)
    ordering = ('label',)

    search_fields = ('label', 'code_insee')

    readonly_fields = ('created', 'modified')
    prepopulated_fields = {'slug': ('label',), }


class SportDisciplineInline(admin.TabularInline):

    model = SportDiscipline
    extra = 0
    can_delete = False

    fields = ('reference', 'label', 'is_active')
    readonly_fields = ('reference', 'label', 'is_active')

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Activity)
class ActivityAdmin(admin.ModelAdmin):

    save_on_top = True

    list_display = ('label', 'is_active', 'created')
    list_filter = ('is_active',)
    ordering = ('label',)

    search_fields = ('reference', 'label',)

    readonly_fields = ('created', 'modified')
    prepopulated_fields = {'slug': ('label',)}

    fieldsets = [
        (None, {'fields': ('reference', 'label', 'slug', 'image', 'description', 'created', 'modified')})
    ]

    inlines = [
        # SportDisciplineInline
    ]


@admin.register(SportDiscipline)
class SportDisciplineAdmin(admin.ModelAdmin):

    save_on_top = True

    list_display = ('label', 'activity', 'is_active',)
    list_filter = ('is_active', 'activity',)
    ordering = ('label',)

    search_fields = ('reference', 'label',)

    readonly_fields = ('created', 'modified')
    prepopulated_fields = {'slug': ('label',)}

    fieldsets = [
        (None, {'fields': ('reference', 'label', 'slug', 'description', 'created', 'modified')}),
        (_(u'Activité'), {'fields': ('activity',)}),
    ]


class ImageInline(GenericTabularInline):
    model = AttachedMedia
    extra = 1


@admin.register(Institution)
class InstitutionAdmin(admin.ModelAdmin):

    save_on_top = True

    list_display = ('code', 'label', 'street_name', 'commune', 'is_active',)
    list_filter = ('is_active', 'created', 'special_institution', 'commune', 'publisher')
    ordering = ('commune__label',)

    search_fields = ('label', 'street_name', 'commune__label')

    readonly_fields = ('created', 'modified', 'medias')
    prepopulated_fields = {'slug': ('label',), }

    raw_id_fields = ('special_institution', 'publisher', 'commune')

    fieldsets = [
        (None, {'fields': ('code', 'label', 'slug', 'cover', 'description',)}),
        (_(u'Administration'), {'fields': ('special_institution', 'publisher', 'is_active', ('created', 'modified'))}),
        (_(u'Adresse'), {'fields': (('street_number', 'street_name'), ('postal_code', 'commune'))}),
        (_(u'Trasports'), {'fields': (('have_metro', 'have_bus', 'have_tramway'),
                                      ('have_train', 'have_boat', 'have_other_transport'))}),
        (_(u'Accéssibilité'), {'fields': ('is_accessible_for_hand_m', 'is_accessible_for_hand_s')}),
    ]

    inlines = [
        # ImageInline,
    ]


@admin.register(Equipment)
class EquipmentAdmin(admin.ModelAdmin):

    list_display = ('label', 'institution', 'category', 'is_public', 'is_active')
    list_filter = ('is_public', 'is_active', 'category',)

    ordering = ('institution__commune', 'institution',)

    readonly_fields = ('created', 'modified')
    prepopulated_fields = {'slug': ('label',), }

    raw_id_fields = ('institution', 'publisher', 'ground', 'category', 'ground', 'environment')

    fieldsets = [
        (None, {'fields': ('code', 'label', 'slug', 'institution', 'cover', 'description',)}),
        (_(u'Administration'), {'fields': ('publisher', 'is_active', ('created', 'modified'))}),
        (_(u'Géolocalisation'), {'fields': ('gps_coordinates',)}),
        (_(u'Informations de base'), {'fields': ('is_public', 'price', ('is_always_open', 'only_season'))}),
        (_(u'Accéssibilité'), {'fields': (('accessible_handicapped_m', 'accessible_handicapped_s'))}),
        (_(u'Informations supplémentaires'), {'fields': (('category', 'ground', 'environment'),
                                                         ('is_erp_cts', 'is_erp_ref', 'is_erp_l', 'is_erp_n', 'is_erp_o'),
                                                         ('is_erp_oa', 'is_erp_p', 'is_erp_pa', 'is_erp_r', 'is_erp_rpe'),
                                                         ('is_erp_sg', 'is_erp_x'))}),
    ]

    default_lon = 2.349014
    default_lat = 48.864716
    default_zoom = 4

    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget}
    }

    inlines = [
        # ImageInline,
    ]


@admin.register(EquipmentSportDiscipline)
class EquipmentSportDisciplineAdmin(admin.ModelAdmin):

    list_display = ('discipline', 'equipment',)
    search_fields = ('discipline', 'equipment',)
    raw_id_fields = ('discipline', 'equipment',)
    ordering = ('discipline', 'equipment',)

    list_filter = ('discipline',)
