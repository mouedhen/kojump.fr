# -*- coding: utf-8 -*-
import os
import logging
from slugify import slugify

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.locations import Department
from equipments.management.helpers.core import ABSCoreCommandImporter

logger = logging.getLogger(__name__)


class DepartmentImporter(ABSCoreCommandImporter):

    def serialize(self, row):
        code = row['code']
        label = row['label']
        slug = slugify(label)
        logger.info('[SUCCESS][SERIALIZE] Record serialized - slug: {}'.format(slug))
        return self.model(code=code, label=label, slug=slug,)


class Command(BaseCommand):

    help = 'Polpulate departments datatable.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating departments datatable')
        families_importer = DepartmentImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '12.departments.csv'),
            model=Department
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] departments end')
