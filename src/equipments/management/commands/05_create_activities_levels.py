# -*- coding: utf-8 -*-
import os
import logging

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.categories import ActivityLevel
from equipments.management.helpers.base import SluggedModelImporter

logger = logging.getLogger(__name__)


class Command(BaseCommand):

    help = 'Populate activities levels data table.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating activities levels data table')
        families_importer = SluggedModelImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '5.activities_levels.csv'),
            model=ActivityLevel
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] activities levels end')
