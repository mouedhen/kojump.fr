# -*- coding: utf-8 -*-
import os
import logging

from slugify import slugify

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.categories import Activity
from equipments.management.helpers.core import ABSCoreCommandImporter

logger = logging.getLogger(__name__)


class ActivitiesImporter(ABSCoreCommandImporter):

    def serialize(self, row):
        activity_id = row['reference']
        label = row['label']
        slug = slugify(row['label'])
        is_active = row['is_active']
        logger.info('[SUCCESS][SERIALIZE][ActivitiesImporter] Record serialized - slug: {}'.format(slug))
        return self.model(pk=activity_id, label=label, slug=slug, is_active=is_active)


class Command(BaseCommand):

    help = 'Populate activities data table.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating activities data table')
        families_importer = ActivitiesImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '3.activities.csv'),
            model=Activity
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] activities populating end')
