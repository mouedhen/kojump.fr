# -*- coding: utf-8 -*-
import os
import logging

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.categories import UsageType
from equipments.management.helpers.base import SluggedModelImporter

logger = logging.getLogger(__name__)


class Command(BaseCommand):

    help = 'Populate equipments usages types data table.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating usages types data table')
        families_importer = SluggedModelImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '8.usages_types.csv'),
            model=UsageType
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] usages types populating end')
