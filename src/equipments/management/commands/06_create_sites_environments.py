# -*- coding: utf-8 -*-
import os
import logging

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.categories import SiteEnvironment
from equipments.management.helpers.base import SluggedModelImporter

logger = logging.getLogger(__name__)


class Command(BaseCommand):

    help = 'Populate equipments sites environments data table.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating sites environments data table')
        families_importer = SluggedModelImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '6.sites_environments.csv'),
            model=SiteEnvironment
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] sites environments populating end')
