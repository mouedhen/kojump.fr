# -*- coding: utf-8 -*-
import os
import logging
from slugify import slugify

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand

from equipments.management.helpers.core import ABSCoreCommandImporter
from equipments.models.categories import SpecialInstitution
from equipments.models.institution import Institution
from equipments.models.locations import Commune, Department

logger = logging.getLogger(__name__)


class InstitutionImporter(ABSCoreCommandImporter):

    def serialize(self, row):
        if row['DepCode'].isdecimal():
            if int(row['DepCode']) > 95:
                return None

        code = row['InsNumeroInstall']
        if code.isdecimal():
            if int(code) == 110050002 \
                    or int(code) == 130010002\
                    or int(code) == 130550026\
                    or int(code) == 191230007\
                    or int(code) == 341870016\
                    or int(code) == 591830020:
                return None

        label = row['InsNom']
        slug = slugify(row['InsNom'])

        try:
            slug_special_institution = 'non' \
                if (slugify(row['InsPartLibelle']) == 'null') or \
                   (slugify(row['InsPartLibelle']).isdecimal()) or \
                   (slugify(row['InsPartLibelle']) == '') else slugify(row['InsPartLibelle'])
            special_institution = SpecialInstitution.objects.get(slug=slug_special_institution)
        except ValueError as error:
            logger.debug(error)
            label_special_institution = row['InsPartLibelle']
            special_institution = SpecialInstitution(
                label=label_special_institution,
                slug=slugify(row['InsPartLibelle']))
            special_institution.save()
            logger.error('[ValueError] special institution does not exist')
            return None
        except ObjectDoesNotExist as error:
            logger.error(
                '[ObjectDoesNotExist] special institution does not exist')
            logger.debug(error)
            label_special_institution = row['InsPartLibelle']
            special_institution = SpecialInstitution(
                label=label_special_institution,
                slug=slugify(row['InsPartLibelle']))
            special_institution.save()
            logger.warning(
                '[ObjectDoesNotExist] special institution created')
            return None

        street_number = row['InsNoVoie']
        street_name = row['InsLibelleVoie']
        postal_code = row['InsCodePostal']

        try:
            commune = Commune.objects.get(code_insee=row['ComInsee'])
        except ValueError as error:
            logger.error(
                '[ValueError] Commune does not exist')
            logger.debug(error)
            code_commune = row['ComInsee']
            label_commune = row['ComLib']
            commune = Commune(code_insee=code_commune, label=label_commune, slug=slugify(row['ComLib']))
            commune.save()
            logger.warning('[ValueError] Commune created')
            return None
        except ObjectDoesNotExist as error:
            logger.error('[ObjectDoesNotExist] Commune does not exist')
            logger.debug(error)
            code_commune = row['ComInsee']
            label_commune = row['ComLib']
            dep_code = row['DepCode']
            dep_label = row['DepLib']
            dep_slug = slugify(row['DepLib'])
            department, dep_created = Department.objects.get_or_create(code=dep_code, label=dep_label, slug=dep_slug)
            commune = Commune(code_insee=code_commune, label=label_commune,
                              slug=slugify(row['ComLib']), department=department)
            commune.save()
            logger.warning('[ObjectDoesNotExist] Commune created')
            return None

        if (not row['InsAccessibiliteHandiMoteur'].isdigit()) \
                or (int(row['InsAccessibiliteHandiMoteur']) > 1):
            is_accessible_for_hand_m = False
        else:
            is_accessible_for_hand_m = row['InsAccessibiliteHandiMoteur']

        if (not row['InsAccessibiliteHandiSens'].isdigit()) \
                or (int(row['InsAccessibiliteHandiSens']) > 1):
            is_accessible_for_hand_s = False
        else:
            is_accessible_for_hand_s = row['InsAccessibiliteHandiSens']

        if (not row['InsTransportMetro'].isdigit()) \
                or (int(row['InsTransportMetro']) > 1):
            have_metro = False
        else:
            have_metro = row['InsTransportMetro']

        if (not row['InsTransportBus'].isdigit()) \
                or (int(row['InsTransportBus']) > 1):
            have_bus = False
        else:
            have_bus = row['InsTransportBus']

        if (not row['InsTransportTram'].isdigit()) \
                or (int(row['InsTransportTram']) > 1):
            have_tramway = False
        else:
            have_tramway = row['InsTransportTram']

        if (not row['InsTransportTrain'].isdigit()) \
                or (int(row['InsTransportTrain']) > 1):
            have_train = False
        else:
            have_train = row['InsTransportTrain']

        if (not row['InsTransportBateau'].isdigit()) \
                or (int(row['InsTransportBateau']) > 1):
            have_boat = False
        else:
            have_boat = row['InsTransportBateau']

        if (not row['InsTransportAutre'].isdigit()) \
                or (int(row['InsTransportAutre']) > 1):
            have_other_transport = False
        else:
            have_other_transport = row['InsTransportAutre']

        logger.info('[SUCCESS][SERIALIZE] record serialized - slug: {}'.format(slug))
        # return None
        return self.model(
            code=code,
            label=label,
            slug=slug,
            special_institution=special_institution,
            street_number=street_number,
            street_name=street_name,
            postal_code=postal_code,
            commune=commune,
            is_accessible_for_hand_m=is_accessible_for_hand_m,
            is_accessible_for_hand_s=is_accessible_for_hand_s,
            have_metro=have_metro,
            have_bus=have_bus,
            have_tramway=have_tramway,
            have_train=have_train,
            have_boat=have_boat,
            have_other_transport=have_other_transport,
            is_active=True
        )


class Command(BaseCommand):

    help = 'Polpulate institutions datatable.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating institutions datatable')
        institutions_importer = InstitutionImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '14.institutions.csv'),
            model=Institution
        )
        institutions_importer.populate()
        logger.info('[SEEDER][POPULATING] institutions end')
