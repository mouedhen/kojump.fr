# -*- coding: utf-8 -*-
import os
import logging

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.categories import SpecialInstitution
from equipments.management.helpers.base import SluggedModelImporter

logger = logging.getLogger(__name__)


class Command(BaseCommand):

    help = 'Polpulate specials institutions datatable.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating specials institutions datatable')
        families_importer = SluggedModelImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '11.special_institutions.csv'),
            model=SpecialInstitution
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] specials institutions populating end')
