# -*- coding: utf-8 -*-
import os
import logging

from slugify import slugify

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.categories import ManagerCategory
from equipments.management.helpers.core import ABSCoreCommandImporter

logger = logging.getLogger(__name__)


class EquipmentCategoryImporter(ABSCoreCommandImporter):

    def serialize(self, row):
        code = row['code']
        label = row['label']
        slug = slugify(label)
        if int(row['is_public']) == 0:
            is_public = False
        elif int(row['is_public']) == 1:
            is_public = True
        else:
            is_public = None
        logger.info('[SUCCESS][SERIALIZE] Record serialized - slug: {}'.format(slug))
        return self.model(code=code, label=label, slug=slug, is_public=is_public)


class Command(BaseCommand):
    help = 'Populate managers categories data table.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating managers categories data table')
        families_importer = EquipmentCategoryImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '10.managers_categories.csv'),
            model=ManagerCategory
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] managers categories populating end')
