# -*- coding: utf-8 -*-
import os
import logging
from slugify import slugify

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.locations import Commune, Department
from equipments.management.helpers.core import ABSCoreCommandImporter

logger = logging.getLogger(__name__)


class CommuneImporter(ABSCoreCommandImporter):

    def serialize(self, row):
        code_insee = row['code_insee']
        label = row['label']
        slug = slugify(label)
        department_code = row['department']
        department = Department.objects.get(code=department_code)
        logger.info('[SUCCESS][SERIALIZE] Record serialized - slug: {}'.format(slug))
        return Commune(code_insee=code_insee, label=label, slug=slug, department=department)


class Command(BaseCommand):

    help = 'Populate departments data table.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating departments data table')
        families_importer = CommuneImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '13.communes.csv'),
            model=Commune
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] departments end')
