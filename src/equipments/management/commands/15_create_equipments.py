# -*- coding: utf-8 -*-
import os
import logging

from django.contrib.gis.geos import Point
from django.core.exceptions import ObjectDoesNotExist
from slugify import slugify

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.locations import Commune
from equipments.models.equipment import Equipment
from equipments.models.categories import Category, ManagerCategory, SiteEnvironment, SiteGround
from equipments.management.helpers.core import ABSCoreCommandImporter
from equipments.models.institution import Institution

logger = logging.getLogger(__name__)


class EquipmentImporter(ABSCoreCommandImporter):

    @staticmethod
    def fill_boolean(column):
        try:
            if column.isdecimal:
                if int(column) >= 0:
                    return True
            return False
        except ValueError:
            return False

    @staticmethod
    def get_commune(column):
        try:
            commune = Commune.objects.get(code_insee=column)
            return commune
        except ValueError as error:
            logger.error('[ValueError] : Commune does not exist')
            logger.debug(error)
            return None
        except ObjectDoesNotExist as error:
            logger.error('[ObjectDoesNotExist] : Commune does not exist')
            logger.debug(error)
            return None

    @staticmethod
    def get_institute(code):
        try:
            return Institution.objects.get(code=code)
        except Institution.DoesNotExist:
            return None

    @staticmethod
    def get_category(column):
        try:
            if column.isdigit():
                category = Category.objects.get(pk=column)
            else:
                category = Category.objects.get(slug=slugify(column))
            return category
        except Category.DoesNotExist:
            return None

    @staticmethod
    def get_ground(column):
        try:
            ground = SiteGround.objects.get(slug=slugify(column))
            return ground
        except SiteGround.DoesNotExist:
            return None

    @staticmethod
    def get_environment(column):
        try:
            environment = SiteEnvironment.objects.get(slug__exact=slugify(column))
            return environment
        except SiteEnvironment.DoesNotExist:
            return None

    @staticmethod
    def get_manager(column):
        try:
            if not column:
                column = 'autre'
            manager = ManagerCategory.objects.get(slug__exact=slugify(column))
            return manager
        except ManagerCategory.DoesNotExist:
            return None

    @staticmethod
    def get_yes_no_choice(column):
        c = slugify(column)
        if c == 'oui':
            return 'yes'
        if c == 'non':
            return 'no'
        return 'not concerned'

    def serialize(self, row):

        code = row['EquipementId']
        if not code.isdecimal():
            return None
        label = row['EquNom']
        slug = slugify(row['EquNom'])

        try:
            latitude = float(row['EquGpsX'].replace(',', '.'))
            longitude = float(row['EquGpsY'].replace(',', '.'))
            gps_coordinates = Point(latitude, longitude, srid=4326)
        except ValueError:
            return None

        is_erp_cts = self.fill_boolean(row['EquErpCTS'])
        is_erp_ref = self.fill_boolean(row['EquErpREF'])
        is_erp_l = self.fill_boolean(row['EquErpL'])
        is_erp_n = self.fill_boolean(row['EquErpN'])
        is_erp_o = self.fill_boolean(row['EquErpO'])
        is_erp_oa = self.fill_boolean(row['EquErpOA'])
        is_erp_p = self.fill_boolean(row['EquErpP'])
        is_erp_pa = self.fill_boolean(row['EquErpPA'])
        is_erp_r = self.fill_boolean(row['EquErpR'])
        is_erp_rpe = self.fill_boolean(row['EquErpRPE'])
        is_erp_sg = self.fill_boolean(row['EquErpSG'])
        is_erp_x = self.fill_boolean(row['EquErpX'])

        is_always_open = self.fill_boolean(row['EquOuvertSaison'])
        only_season = self.fill_boolean(row['EquProximite'])

        for_schools_use = self.fill_boolean(row['EquUtilScolaire'])
        for_clubs_use = self.fill_boolean(row['EquUtilClub'])
        for_others_use = self.fill_boolean(row['EquUtilAutre'])
        for_individual_use = self.fill_boolean(row['EquUtilIndividuel'])

        accessible_handicapped_m = self.get_yes_no_choice(row['EquAccesHandimAire'])
        accessible_handicapped_s = self.get_yes_no_choice(row['EquAccesHandisAire'])

        # Related models
        commune = self.get_commune(row['ComInsee'])
        if not commune:
            logger.info('[FAIL][SERIALIZE][COMMUNE] Equipment - slug: {}'.format(slug))
            return None

        manager = self.get_manager(row['GestionTypeGestionnairePrincLib'])
        if not manager:
            logger.info('[FAIL][SERIALIZE][MANAGER] Equipment - slug: {}'.format(slug))
            return None

        is_public = manager.is_public

        institution = self.get_institute(row['InsNumeroInstall'])
        if not institution:
            logger.info('[FAIL][SERIALIZE][INSTITUTION] Equipment - slug: {}'.format(slug))
            return None

        category = self.get_category(row['EquipementTypeCode'])
        if not category:
            logger.info('[FAIL][SERIALIZE][CATEGORY] Equipment - slug: {}'.format(slug))

        ground = self.get_ground(row['NatureSolLib'])
        if not ground:
            logger.info('[FAIL][SERIALIZE][GROUND] Equipment - slug: {}'.format(slug))

        environment = self.get_environment(row['NatureLibelle'])
        if not environment:
            logger.info('[FAIL][SERIALIZE][ENVIRONMENT] Equipment - slug: {}'.format(slug))

        logger.info('[SUCCESS][SERIALIZE] Equipment - slug: {}'.format(slug))

        return self.model(
            code=code, label=label, slug=slug,
            commune=commune,
            is_public=is_public,
            manager=manager,
            gps_coordinates=gps_coordinates,
            is_erp_l=is_erp_l, is_erp_n=is_erp_n, is_erp_o=is_erp_o,
            is_erp_oa=is_erp_oa, is_erp_p=is_erp_p, is_erp_pa=is_erp_pa,
            is_erp_r=is_erp_r, is_erp_rpe=is_erp_rpe, is_erp_sg=is_erp_sg,
            is_erp_x=is_erp_x, is_erp_cts=is_erp_cts, is_erp_ref=is_erp_ref,
            is_always_open=is_always_open, only_season=only_season,
            for_schools_use=for_schools_use, for_clubs_use=for_clubs_use,
            for_others_use=for_others_use, for_individual_use=for_individual_use,
            accessible_handicapped_m=accessible_handicapped_m, accessible_handicapped_s=accessible_handicapped_s,
            institution=institution, category=category, ground=ground, environment=environment,
            is_active=True
        )


class Command(BaseCommand):
    help = 'Polpulate equipments datatable.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating equipments datatable')
        equipments_importer = EquipmentImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '15.equipments.csv'),
            model=Equipment
        )
        equipments_importer.populate()
        logger.info('[SEEDER][POPULATING] equipments end')
