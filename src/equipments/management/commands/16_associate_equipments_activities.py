# -*- coding: utf-8 -*-
import os
import logging

from django.core.management import BaseCommand

from equipments.management.helpers.core import ABSCoreCommandImporter
from equipments.models.categories import SportDiscipline
from equipments.models.equipment import Equipment, EquipmentSportDiscipline
from django.conf import settings

logger = logging.getLogger(__name__)


class EquipmentDisciplinesAssociate(ABSCoreCommandImporter):

    def serialize(self, row):
        try:
            equipment = Equipment.objects.get(code=row['EquipementId'])
        except ValueError:
            logger.info('[ASSOCIATION][EQUIPMENT][FAIL]')
            return None
        except Equipment.DoesNotExist:
            logger.info('[ASSOCIATION][EQUIPMENT][FAIL]')
            return None
        try:
            discipline = SportDiscipline.objects.get(code=row['ActCode'])
        except ValueError:
            logger.info('[ASSOCIATION][DISCIPLINE][FAIL]')
            return None
        except SportDiscipline.DoesNotExist:
            logger.info('[ASSOCIATION][DISCIPLINE][FAIL]')
            return None
        logger.info('{} - {}'.format(equipment.slug, discipline.slug))
        return self.model(equipment=equipment, discipline=discipline)


class Command(BaseCommand):

    help = 'Populate associate equipments to disciplines'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start associate equipments to disciplines')
        association_importer = EquipmentDisciplinesAssociate(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '16.equipments_activities.csv'),
            model=EquipmentSportDiscipline
        )
        association_importer.populate()
        logger.info('[SEEDER][POPULATING] associate equipments to disciplines end')
