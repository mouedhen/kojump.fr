# -*- coding: utf-8 -*-
import os
import logging

from slugify import slugify

from django.core.management.base import BaseCommand
from django.conf import settings

from equipments.models.categories import Activity, SportDiscipline
from equipments.management.helpers.core import ABSCoreCommandImporter

logger = logging.getLogger(__name__)


class SportDisciplineImporter(ABSCoreCommandImporter):

    def serialize(self, row):
        code = row['reference']
        label = row['label']
        slug = slugify(row['label'])
        activity_ref = row['activity_reference']
        if int(activity_ref) > 89:
            activity_ref = str(89)
        activity = Activity.objects.get(pk=activity_ref)
        is_active = activity.is_active
        logger.info('[SUCCESS][SERIALIZE][SportDisciplineImporter] Record serialized - slug: {}'.format(slug))
        return self.model(code=code, label=label, slug=slug, is_active=is_active, activity=activity)


class Command(BaseCommand):

    help = 'Populate sports disciplines data table.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating sports disciplines data table')
        families_importer = SportDisciplineImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '4.sports_disciplines.csv'),
            model=SportDiscipline
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] sports disciplines populating end')
