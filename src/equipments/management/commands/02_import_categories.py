import logging
import os
from slugify import slugify

from django.conf import settings
from django.core.management import BaseCommand

from equipments.management.helpers.core import ABSCoreCommandImporter
from equipments.models.categories import Category

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class CategoryImporter(ABSCoreCommandImporter):

    def serialize(self, row):
        record_id = row['reference']
        label = row['label']
        slug = slugify(label)
        family_id = row['family_reference']
        logger.info('[SUCCESS][SERIALIZE][CategoryImporter] Record created - slug: {}'.format(slug))
        return self.model(id=record_id, label=label, slug=slug, family_id=family_id)


class Command(BaseCommand):
    help = 'Populate equipments categories families data table.'

    def handle(self, *args, **options):
        logging.debug('[SEEDER] start populating categories families data table')
        families_importer = CategoryImporter(
            filename=os.path.join(settings.BASE_DIR, '../..', 'data', '2.categories.csv'),
            model=Category
        )
        families_importer.populate()
        logger.info('[SEEDER][POPULATING] families end')
