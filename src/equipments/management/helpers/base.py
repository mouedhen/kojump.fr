import logging
from slugify import slugify

from .core import ABSCoreCommandImporter

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class BaseImporter(ABSCoreCommandImporter):

    def serialize(self, row):
        """
        Serialize row to model
        :param row:
        :return: models.Model
        """
        record_id = row['reference']
        label = row['label']
        slug = slugify(label)
        logger.info('[SUCCESS][SERIALIZE] Record created - slug: {}'.format(slug))
        return self.model(id=record_id, label=label, slug=slug)


class SluggedModelImporter(ABSCoreCommandImporter):

    def serialize(self, row):
        """
        Serialize row to model
        :param row:
        :return: models.Model
        """
        label = row['label']
        slug = slugify(row['label'])
        logger.info('[SUCCESS][SERIALIZE] Record created - slug: {}'.format(slug))
        return self.model(label=label, slug=slug)
