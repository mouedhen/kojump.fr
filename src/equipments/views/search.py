# -*- coding: utf-8 -*-
from django.shortcuts import redirect
from django.views.generic import TemplateView
from equipments.forms.activity import MultiCheckboxActivitiesForm
from equipments.models.categories import Activity


class SearchView(TemplateView):

    template_name = 'search/search.html'

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        context['form_activities'] = MultiCheckboxActivitiesForm
        return context

    def get(self, request, *args, **kwargs):
        if request.GET.get('previous') == '/':
            filters = dict(request.GET)
            if 'activities' in filters:
                activities = filters['activities']
                if len(activities) == 1:
                    activity = Activity.objects.get(pk=activities[0])
                    return redirect(activity.get_absolute_url())
        return super(SearchView, self).get(request, *args, **kwargs)
