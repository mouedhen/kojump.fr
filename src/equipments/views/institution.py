# -*- coding: utf-8 -*-
from django.views.generic import View

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, ListView

from equipments.models.categories import Activity
from equipments.models.equipment import Equipment
from equipments.models.institution import Institution
from equipments.models.socials import GlobalRate, HygieneRate, EquipmentsRate, ServicesRate
from publishing.models.publishing import Comment, Favorite
from publishing.forms.comment import CreateCommentForm
# from equipments.forms.socials import GlobalRatingForm, HygieneRatingForm, EquipmentsRatingForm, ServicesRatingForm

from braces.views import (AjaxResponseMixin, JSONResponseMixin)

from core.models.attachement import AttachedMedia


class InstitutionPhotoUploadView(JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            error_dict = {'message': 'Only logged in users can upload files.'}
            return self.render_json_response(error_dict, status=401)
        try:
            institution = Institution.objects.get(pk=kwargs.get('pk'))
        except Institution.DoesNotExist:
            error_dict = {'message': 'Institution not found.'}
            return self.render_json_response(error_dict, status=404)
        uploaded_file = request.FILES['file']
        AttachedMedia.objects.create(
            image=uploaded_file,
            content_type=ContentType.objects.get_for_model(institution),
            object_id=institution.pk,
        )
        response_dict = {
            'message': 'File uploaded successfully!',
        }
        return self.render_json_response(response_dict, status=200)


class EquipmentPhotoUploadView(JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            error_dict = {'message': 'Unauthorized.'}
            return self.render_json_response(error_dict, status=401)
        try:
            equipment = Equipment.objects.get(pk=kwargs.get('pk'))
        except Equipment.DoesNotExist:
            error_dict = {'message': 'Equipment not found.'}
            return self.render_json_response(error_dict, status=404)
        uploaded_file = request.FILES['file']
        AttachedMedia.objects.create(
            image=uploaded_file,
            content_type=ContentType.objects.get_for_model(equipment),
            object_id=equipment.pk,
        )
        response_dict = {
            'message': 'File uploaded successfully!',
        }
        return self.render_json_response(response_dict, status=200)


class EquipmentPublicDetailView(DetailView):
    model = Equipment
    template_name = 'equipments/equipment-public.html'

    def get_context_data(self, **kwargs):
        context = super(EquipmentPublicDetailView, self).get_context_data(**kwargs)
        context['form_comment'] = CreateCommentForm
        context['activities'] = Activity.objects.filter(disciplines__equipments=context['equipment']).distinct()
        # context['activities'] = context['equipment'].disciplines.activity.distinct()
        return context

    @method_decorator(login_required)
    def post(self, request, pk, slug):
        # post = Post.objects.get(institution=self.get_object())
        obj = self.get_object()
        comment = Comment(
            publisher=request.user,
            content_type=ContentType.objects.get_for_model(obj),
            object_id=obj.pk,
        )

        if request.POST['global-rate'] and int(request.POST['global-rate']) > 0:
            global_rating = GlobalRate(
                publisher=request.user,
                content_type=ContentType.objects.get_for_model(obj),
                object_id=obj.pk,
                rating=request.POST['global-rate'],
            )
            global_rating.save()

        if request.POST['hygiene-rate'] and int(request.POST['hygiene-rate']) > 0:
            hygiene_rating = HygieneRate(
                publisher=request.user,
                content_type=ContentType.objects.get_for_model(obj),
                object_id=obj.pk,
                rating=request.POST['hygiene-rate'],
            )
            hygiene_rating.save()

        if request.POST['services-rate'] and int(request.POST['services-rate']) > 0:
            services_rating = ServicesRate(
                publisher=request.user,
                content_type=ContentType.objects.get_for_model(obj),
                object_id=obj.pk,
                rating=request.POST['services-rate'],
            )
            services_rating.save()

        if request.POST['equipments-rate'] and int(request.POST['equipments-rate']) > 0:
            equipments_rating = EquipmentsRate(
                publisher=request.user,
                content_type=ContentType.objects.get_for_model(obj),
                object_id=obj.pk,
                rating=request.POST['equipments-rate'],
            )
            equipments_rating.save()

        comment_form = CreateCommentForm(request.POST, instance=comment)
        if comment_form.is_valid():
            comment_form.save()
        return redirect(self.get_success_url())

    def get_success_url(self):
        return self.get_object().get_absolute_url()


class InstitutionPublicDetailView(DetailView):
    model = Institution
    template_name = 'equipments/institution-public.html'

    def get_context_data(self, **kwargs):
        context = super(InstitutionPublicDetailView, self).get_context_data(**kwargs)
        # TODO only active equipments
        context['equipments'] = context['institution'] \
            .equipments.filter(disciplines__is_active=True) \
            .distinct()
        context['form_comment'] = CreateCommentForm
        return context

    @method_decorator(login_required)
    def post(self, request, pk, slug):
        # post = Post.objects.get(institution=self.get_object())
        obj = self.get_object()
        comment = Comment(
            publisher=request.user,
            content_type=ContentType.objects.get_for_model(obj),
            object_id=obj.pk,
        )
        if request.POST['global-rate'] and int(request.POST['global-rate']) > 0:
            global_rating = GlobalRate(
                publisher=request.user,
                content_type=ContentType.objects.get_for_model(obj),
                object_id=obj.pk,
                rating=request.POST['global-rate'],
            )
            global_rating.save()

        if request.POST['hygiene-rate'] and int(request.POST['hygiene-rate']) > 0:
            hygiene_rating = HygieneRate(
                publisher=request.user,
                content_type=ContentType.objects.get_for_model(obj),
                object_id=obj.pk,
                rating=request.POST['hygiene-rate'],
            )
            hygiene_rating.save()

        if request.POST['services-rate'] and int(request.POST['services-rate']) > 0:
            services_rating = ServicesRate(
                publisher=request.user,
                content_type=ContentType.objects.get_for_model(obj),
                object_id=obj.pk,
                rating=request.POST['services-rate'],
            )
            services_rating.save()

        if request.POST['equipments-rate'] and int(request.POST['equipments-rate']) > 0:
            equipments_rating = EquipmentsRate(
                publisher=request.user,
                content_type=ContentType.objects.get_for_model(obj),
                object_id=obj.pk,
                rating=request.POST['equipments-rate'],
            )
            equipments_rating.save()

        comment_form = CreateCommentForm(request.POST, instance=comment)

        # global_rating_form = GlobalRatingForm(request.POST, instance=global_rating)
        # hygiene_rating = HygieneRatingForm(request.POST, instance=hygiene_rating)
        # services_rating_form = ServicesRatingForm(ratingrequest.POST, instance=services_rating)
        # equipments_rating_form = EquipmentsRatingForm(request.POST, instance=equipments_rating)

        if comment_form.is_valid():
            comment_form.save()

        # if global_rating_form.is_valid(): global_rating_form.save()
        # if hygiene_rating.is_valid(): hygiene_rating.save()
        # if services_rating_form.is_valid(): services_rating_form.save()
        # if equipments_rating_form.is_valid(): equipments_rating_form.save()

        return redirect(self.get_success_url())

    def get_success_url(self):
        return self.get_object().get_absolute_url()


class FavoriteInstitutions(LoginRequiredMixin, ListView):
    model = Institution
    template_name = 'equipments/institutions-list.html'

    def get_queryset(self):
        # favorites = Favorite.objects.filter(publisher=self.request.user)
        return Institution.objects.filter(
            favorites__in=Favorite.objects.filter(publisher=self.request.user)
        ).distinct()


class FavoriteEquipments(LoginRequiredMixin, ListView):
    model = Equipment
    template_name = 'equipments/equipments-list.html'

    def get_queryset(self):
        # favorites = Favorite.objects.filter(publisher=self.request.user)
        return Equipment.objects.filter(
            favorites__in=Favorite.objects.filter(publisher=self.request.user)
        ).distinct()


class CommentedInstitutions(LoginRequiredMixin, ListView):
    model = Institution
    template_name = 'equipments/institutions-list.html'

    def get_queryset(self):
        return Institution.objects.filter(
            comments__in=Comment.objects.filter(publisher=self.request.user, )
        ).distinct()
