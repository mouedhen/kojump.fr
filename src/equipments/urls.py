# -*- coding: utf-8 -*-
from django.urls import path, re_path

from .views.activity import ActivityDetailView
from .views.institution import (InstitutionPublicDetailView,
                                FavoriteEquipments,
                                FavoriteInstitutions,
                                CommentedInstitutions,
                                EquipmentPublicDetailView,
                                InstitutionPhotoUploadView,
                                EquipmentPhotoUploadView)
from .views.search import SearchView

app_name = 'equipments'

urlpatterns = [

    re_path(r'activities/(?P<pk>[0-9]+)/(?P<slug>[-\w]+)/$',
            ActivityDetailView.as_view(),
            name='activity-details'),

    re_path(r'institutions/(?P<pk>[0-9]+)/(?P<slug>[-\w]+)/$', InstitutionPublicDetailView.as_view(),
            name='institution-details'),

    re_path(r'equipments/(?P<pk>[0-9]+)/(?P<slug>[-\w]+)/$', EquipmentPublicDetailView.as_view(),
            name='equipment-details'),

    path('institutions/favorites/', FavoriteInstitutions.as_view(), name='institutions-favorites'),
    path('equipments/favorites/', FavoriteEquipments.as_view(), name='equipments-favorites'),
    path('institutions/commented/', CommentedInstitutions.as_view(), name='institutions-commented'),

    path('search/', SearchView.as_view(), name='search'),

    re_path(
        r'^(?P<pk>\d+)/institution-upload/$',
        view=InstitutionPhotoUploadView.as_view(),
        name='institution_photo_upload_view',
    ),

    re_path(
        r'^(?P<pk>\d+)/equipment-upload/$',
        view=EquipmentPhotoUploadView.as_view(),
        name='equipment_photo_upload_view',
    ),
]
