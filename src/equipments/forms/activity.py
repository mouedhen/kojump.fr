# -*- coding: utf-8 -*-
from django import forms

from equipments.models.categories import Activity


class MultiSelectActivitiesForm(forms.Form):

    activities = forms.ModelMultipleChoiceField(widget=forms.SelectMultiple(),
                                                queryset=Activity.objects.filter(is_active=True).order_by('label'))


class MultiCheckboxActivitiesForm(forms.Form):

    activities = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(),
                                                queryset=Activity.objects.filter(is_active=True).order_by('label'))
