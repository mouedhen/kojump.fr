# -*- coding: utf-8 -*-
import random

from django.db import models
from django.urls import reverse
from django.utils.encoding import python_2_unicode_compatible
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _

from django.contrib.contenttypes.fields import GenericRelation

from core.models.abstracts import SluggedAbstractModel, DescribedAbstractModel
from core.models.attachement import AttachedMedia
from equipments.models.locations import Commune
from geolocation.models import GeoLocationAbstractModel
from equipments.models.categories import SportDiscipline, SiteEnvironment, SiteGround, Category, ManagerCategory
from equipments.models.institution import Institution
from equipments.models.socials import GlobalRate, HygieneRate, ServicesRate, EquipmentsRate
from publishing.models.abstracts import PostableAbstractModel
from publishing.models.publishing import Comment, Favorite


def generate_color():
    color = '#{:02x}{:02x}{:02x}'.format(*map(lambda x: random.randint(0, 255), range(3)))
    return color


@python_2_unicode_compatible
class Equipment(SluggedAbstractModel, DescribedAbstractModel, PostableAbstractModel,
                GeoLocationAbstractModel):

    YES_NO_CHOICES = (
        ('yes', _(u'oui')),
        ('no', _(u'non')),
        ('not concerned', _(u'non concerné')),
    )

    # General
    code = models.CharField(_('code'), max_length=12, unique=True)
    cover = models.ImageField(
        _('couverture'), blank=True, null=True,
        default='default-institution-thumbnail.png', upload_to='institution/cover',
        help_text=_(u'Cette image est utilisé en tant que couverture pour l\'institution')
    )
    # Address
    commune = models.ForeignKey(
        Commune, on_delete=models.CASCADE, related_name='equipments',
        blank=True, verbose_name='commune', null=True
    )
    # Manager category
    manager = models.ForeignKey(
        ManagerCategory, on_delete=models.CASCADE, related_name='equipments',
        blank=True, verbose_name='type de gestionnaire', null=True
    )
    # Additional category information
    is_erp_cts = models.BooleanField(_('Chapitau tente ?'), default=False, blank=True)
    is_erp_ref = models.BooleanField(_('Etablissement flottant ?'), default=False, blank=True)
    is_erp_l = models.BooleanField(_('Salle polyvalente ?'), default=False, blank=True)
    is_erp_n = models.BooleanField(_('Restaurant et débit de boisson ?'), default=False, blank=True)
    is_erp_o = models.BooleanField(_('Hôtel ?'), default=False, blank=True)
    is_erp_oa = models.BooleanField(_('Hôtel restaurant d\'altitude ?'), default=False, blank=True)
    is_erp_p = models.BooleanField(_('Salle de danse et jeux ?'), default=False, blank=True)
    is_erp_pa = models.BooleanField(_('Etablissement en plein air ?'), default=False, blank=True)
    is_erp_r = models.BooleanField(_('Enseignement et colo ?'), default=False, blank=True)
    is_erp_rpe = models.BooleanField(_('PE ?'), default=False, blank=True)
    is_erp_sg = models.BooleanField(_('Structure gonflable ?'), default=False, blank=True)
    is_erp_x = models.BooleanField(_('Etablissement sportif couvert ?'), default=False, blank=True)
    # Utils
    is_always_open = models.BooleanField(_('7d/7d - 24h/24h ?'), default=False, blank=True)
    only_season = models.BooleanField(_('ouvert selement dans les saisons ?'), default=False, blank=True)
    # Target
    for_schools_use = models.BooleanField(_('pour utilisation scolaire ?'), default=False, blank=True)
    for_clubs_use = models.BooleanField(_('pour utilisation des clubs ?'), default=False, blank=True)
    for_others_use = models.BooleanField(_('pour utilisation autre ?'), default=False, blank=True)
    for_individual_use = models.BooleanField(_('pour utilisation individuelle ?'), default=False, blank=True)
    # Disabled person accessibility information
    accessible_handicapped_m = models.CharField(_('accéssible aux handicapés à mobilité réduite ?'), max_length=14, default='not concerned', choices=YES_NO_CHOICES)
    accessible_handicapped_s = models.CharField(_('accéssible aux handicapés sensoriaux ?'), max_length=14, default='not concerned', choices=YES_NO_CHOICES)
    # Additional information
    is_public = models.NullBooleanField(_('est publique ?'))
    price = models.TextField(_('liste des prix'), blank=True, null=True)

    institution = models.ForeignKey(Institution, on_delete=models.CASCADE, related_name='equipments', blank=True, null=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, related_name='equipments', blank=True, null=True)
    disciplines = models.ManyToManyField(SportDiscipline, related_name='equipments', through='EquipmentSportDiscipline')
    ground = models.ForeignKey(SiteGround, on_delete=models.SET_NULL, related_name='equipments', blank=True, null=True)
    environment = models.ForeignKey(SiteEnvironment, on_delete=models.SET_NULL, blank=True, null=True)

    # Social interactions
    medias = GenericRelation(AttachedMedia)
    comments = GenericRelation(Comment)
    favorites = GenericRelation(Favorite)
    global_rating = GenericRelation(GlobalRate)
    hygiene_rating = GenericRelation(HygieneRate)
    services_rating = GenericRelation(ServicesRate)
    equipments_rating = GenericRelation(EquipmentsRate)

    class Meta:
        verbose_name = _('equipement sportif')
        verbose_name_plural = _('equipements sportifs')

    @cached_property
    def pin_color(self):
        discipline = self.disciplines.first()
        if discipline:
            return discipline.pin_color
        return generate_color()

    @cached_property
    def global_rate(self):
        rate = Equipment.objects.filter(id=self.id).aggregate(
            models.Avg('global_rating__rating'))['global_rating__rating__avg']
        if rate:
            return int(rate)
        return 0

    @cached_property
    def hygiene_rate(self):
        rate = Equipment.objects.filter(id=self.id).aggregate(
            models.Avg('hygiene_rating__rating'))['hygiene_rating__rating__avg']
        if rate:
            return int(rate)
        return 0

    @cached_property
    def services_rate(self):
        rate = Equipment.objects.filter(id=self.id).aggregate(
            models.Avg('services_rating__rating'))['services_rating__rating__avg']
        if rate:
            return int(rate)
        return 0

    @cached_property
    def equipments_rate(self):
        rate = Equipment.objects.filter(id=self.id).aggregate(
            models.Avg('equipments_rating__rating'))['equipments_rating__rating__avg']
        if rate:
            return int(rate)
        return 0

    def __str__(self):
        return '{} - {}'.format(self.label, self.institution.label)

    def image(self):
        return self.cover.url

    def get_absolute_url(self):
        return reverse('equipments:equipment-details', kwargs={'pk': self.pk, 'slug': self.slug})


class EquipmentSportDiscipline(models.Model):
    equipment = models.ForeignKey(Equipment, on_delete=models.CASCADE)
    discipline = models.ForeignKey(SportDiscipline, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('equipement / discipline association')
        verbose_name_plural = _('equipements / disciplines associations')
