from django.db import models
from django.utils.translation import ugettext_lazy as _


from .base import GenericAbstractModel


class Department(GenericAbstractModel):
    class Meta:
        verbose_name = _('département')
        verbose_name_plural = _('départements')
        ordering = ('label',)

    code = models.CharField(_('code département'), max_length=4, unique=True)


class Commune(GenericAbstractModel):
    class Meta:
        verbose_name = _('commune')
        verbose_name_plural = _('communes')
        ordering = ('label',)

    code_insee = models.CharField(_('code INSEE'), max_length=6, unique=True)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, related_name='communes')
