from publishing.models.publishing import Rate


class GlobalRate(Rate):
    pass


class HygieneRate(Rate):
    pass


class ServicesRate(Rate):
    pass


class EquipmentsRate(Rate):
    pass
