# -*- coding: utf-8 -*-
import random

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils.encoding import python_2_unicode_compatible
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _

from django.contrib.contenttypes.fields import GenericRelation

from core.models.abstracts import SluggedAbstractModel
from core.models.attachement import AttachedMedia
from equipments.models.categories import SpecialInstitution
from equipments.models.locations import Commune
from equipments.models.socials import GlobalRate, HygieneRate, ServicesRate, EquipmentsRate
from publishing.models.abstracts import PostableAbstractModel
from publishing.models.publishing import Comment, Favorite, UserRequest


@python_2_unicode_compatible
class Institution(SluggedAbstractModel, PostableAbstractModel):
    # General
    code = models.CharField(_('code'), max_length=20, unique=True)
    description = models.TextField(_('code'), blank=True)
    cover = models.ImageField(
        _('couverture'), blank=True, null=True,
        default='default-institution-thumbnail.png', upload_to='institution/cover',
        help_text=_(u'Cette image est utilisé en tant que couverture pour l\'institution')
    )

    # Administration
    special_institution = models.ForeignKey(
        SpecialInstitution, on_delete=models.CASCADE,
        related_name='institutions', blank=True, verbose_name=_(u'institution spéciale')
    )

    # Address
    street_number = models.CharField(_(u'num. rue'), max_length=10, blank=True, default='_')
    street_name = models.CharField(_('nom de la rue'), max_length=150, blank=True)
    postal_code = models.CharField(_('code postal'), max_length=100, blank=True)
    commune = models.ForeignKey(
        Commune, on_delete=models.CASCADE, related_name='institutions',
        blank=True, verbose_name='commune'
    )

    # Transport information
    have_metro = models.BooleanField(_('accéssible en métro ?'), blank=True, default=False)
    have_bus = models.BooleanField(_('accéssible en autobus ?'), blank=True, default=False)
    have_tramway = models.BooleanField(_('accéssible en tramway ?'), blank=True, default=False)
    have_train = models.BooleanField(_('accéssible en train ?'), blank=True, default=False)
    have_boat = models.BooleanField(_('accéssible en bateau ?'), blank=True, default=False)
    have_other_transport = models.BooleanField(_('a transport (autre) ?'), blank=True, default=False)

    # Disabled person accessibility information
    is_accessible_for_hand_m = models.BooleanField(_('accéssible aux handicapés à mobilité réduite ?'),
                                                   default=False)
    is_accessible_for_hand_s = models.BooleanField(_('accéssible aux handicapés sensoriaux ?'), default=False)

    # Social interactions
    medias = GenericRelation(AttachedMedia)
    comments = GenericRelation(Comment)
    global_rating = GenericRelation(GlobalRate)
    hygiene_rating = GenericRelation(HygieneRate)
    services_rating = GenericRelation(ServicesRate)
    equipments_rating = GenericRelation(EquipmentsRate)

    favorites = GenericRelation(Favorite)
    property_request = GenericRelation(UserRequest)

    class Meta:
        verbose_name = _(u'institution')
        verbose_name_plural = _(u'institutions')
        ordering = ('code',)

    def __str__(self):
        return '{} - {}, {} - {}'.format(self.code, self.label, self.commune, self.commune.department)

    @cached_property
    def global_rate(self):
        rate = Institution.objects.filter(id=self.id).aggregate(
            models.Avg('global_rating__rating'))['global_rating__rating__avg']
        if rate:
            return int(rate)
        return 0

    @cached_property
    def hygiene_rate(self):
        rate = Institution.objects.filter(id=self.id).aggregate(
            models.Avg('hygiene_rating__rating'))['hygiene_rating__rating__avg']
        if rate:
            return int(rate)
        return 0

    @cached_property
    def services_rate(self):
        rate = Institution.objects.filter(id=self.id).aggregate(
            models.Avg('services_rating__rating'))['services_rating__rating__avg']
        if rate:
            return int(rate)
        return 0

    @cached_property
    def equipments_rate(self):
        rate = Institution.objects.filter(id=self.id).aggregate(
            models.Avg('equipments_rating__rating'))['equipments_rating__rating__avg']
        if rate:
            return int(rate)
        return 0

    @cached_property
    def address(self):
        return '{} {} - {} {}, {}'.format(self.street_number, self.street_name,
                                          self.postal_code, self.commune.label, self.commune.department.label)

    @cached_property
    def coordinates(self):
        equipment = self.equipments.first()
        if equipment:
            return equipment.gps_coordinates
        return None

    @cached_property
    def color(self):
        palette = ['#556270', '#4ECDC4', '#C7F464', '#FF6B6B', '#C44D58']
        r = random.randint(0, 4)
        return palette[r]

    def image(self):
        return self.cover.url

    def get_absolute_url(self):
        return reverse('equipments:institution-details', kwargs={'pk': self.pk, 'slug': self.slug})


@receiver(post_save, sender=UserRequest)
def auto_change_proprietary(sender, instance, **kwargs):
    if instance.status == 'SUCCESS' \
            and instance.obj == 'INSTITUTION' \
            and instance.subject == 'PROPERTY':
        institution = Institution.objects.get(code=instance.object_id)
        institution.publisher = instance.publisher
        institution.save()
        return
    return
