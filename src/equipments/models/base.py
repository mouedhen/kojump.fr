from core.models.abstracts import ActivatedAbstractModel, SluggedAbstractModel, TimeStampedAbstractModel


class GenericAbstractModel(ActivatedAbstractModel, SluggedAbstractModel, TimeStampedAbstractModel):
    class Meta:
        abstract = True

    def __str__(self):
        return self.label
