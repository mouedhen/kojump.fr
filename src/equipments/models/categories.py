from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from stdimage import StdImageField
from stdimage.utils import UploadToClassNameDirUUID

from .base import GenericAbstractModel


class CategoryFamily(GenericAbstractModel):
    class Meta:
        verbose_name = _('catégorie - famille')
        verbose_name_plural = _('catégorie - familles')
        ordering = ('label',)


class Category(GenericAbstractModel):
    class Meta:
        verbose_name = _('catégorie')
        verbose_name_plural = _('catégories')
        ordering = ('label',)

    family = models.ForeignKey(CategoryFamily, on_delete=models.CASCADE, related_name='categories')


class Activity(GenericAbstractModel):
    class Meta:
        verbose_name = _('activité')
        verbose_name_plural = _('activités')
        ordering = ('label',)

    image = StdImageField(
        upload_to=UploadToClassNameDirUUID(),
        blank=True,
        variations={
            'thumbnail': (300, 400, True),
        })

    def get_absolute_url(self):
        return reverse('equipments:activity-details', kwargs={'pk': self.pk, 'slug': self.slug})


class SportDiscipline(GenericAbstractModel):
    class Meta:
        verbose_name = _('discipline sportive')
        verbose_name_plural = _('disciplines sportives')
        ordering = ('label',)

    code = models.IntegerField(unique=True)
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE, related_name='disciplines')
    image = StdImageField(
        upload_to=UploadToClassNameDirUUID(),
        blank=True,
        variations={
            'thumbnail': (300, 400, True),
        })


class ActivityLevel(GenericAbstractModel):
    class Meta:
        verbose_name = _('niveau activité')
        verbose_name_plural = _('niveaux activités')
        ordering = ('label',)


class SiteEnvironment(GenericAbstractModel):
    class Meta:
        verbose_name = _('environnement du site')
        verbose_name_plural = _('environnements des sites')
        ordering = ('label',)


class SiteGround(GenericAbstractModel):
    class Meta:
        verbose_name = _('type terrain')
        verbose_name_plural = _('types terrains')
        ordering = ('label',)


class UsageType(GenericAbstractModel):
    class Meta:
        verbose_name = _('type d\'usage')
        verbose_name_plural = _('types d\'usages')
        ordering = ('label',)


class EquipmentTarget(GenericAbstractModel):
    class Meta:
        verbose_name = _('cible')
        verbose_name_plural = _('cibles')
        ordering = ('label',)


class ManagerCategory(GenericAbstractModel):
    class Meta:
        verbose_name = _('catégorie de gestionnaire')
        verbose_name_plural = _('catégories des gestionnaire')
        ordering = ('label',)

    code = models.IntegerField(unique=True)
    is_public = models.NullBooleanField(null=True)


class SpecialInstitution(GenericAbstractModel):
    class Meta:
        verbose_name = _('catégorie des institutions spéciales')
        verbose_name_plural = _('catégories des institutions spéciales')
        ordering = ('label',)
