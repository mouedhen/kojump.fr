from django.db import models
from django.db.models.signals import post_save
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.models import User

from stdimage import StdImageField
from stdimage.utils import UploadToClassNameDirUUID

from core.models.abstracts import ActivatedAbstractModel, LocatedAbstractModel


class Profile(ActivatedAbstractModel, LocatedAbstractModel):
    GENDER_CHOICES = (
        ('M', _('homme')),
        ('F', _('femme')),
        ('N', _('non spécifié')),
    )
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,16}$', message=_('le numéro doit avoir la forme : \'+999999999\''))

    # Authentication
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # General
    photo = StdImageField(
        upload_to=UploadToClassNameDirUUID(),
        blank=True,
        variations={
            'thumbnail': (300, 400, True),
        })
    birth_date = models.DateField(_('date de naissance'), blank=True, null=True)
    gender = models.CharField(_('sexe'), max_length=1, choices=GENDER_CHOICES, default='N')
    bio = models.TextField(_('présentation'), max_length=1024, null=True, blank=True)
    # Contact
    phone_number = models.CharField(_('téléphone'), max_length=16, blank=True, null=True, validators=[phone_regex])
    mobile_number = models.CharField(_('mobile'), max_length=16, blank=True, null=True, validators=[phone_regex])
    fax_number = models.CharField(_('fax'), max_length=16, blank=True, null=True, validators=[phone_regex])
    # Administration
    email_confirmed = models.BooleanField(_('adresse e-mail confirmé ?'), default=False)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = _('profil utilisateur')
        verbose_name_plural = _('profils utilisateurs')


@receiver(post_save, sender=User)
def create_profile_handler(sender, instance, created, **kwargs):
    """
    Handle profile creation on user creation
    """
    if not created:
        return None
    profile = Profile(user=instance)
    profile.save()


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


@receiver(post_save, sender=User)
def unique_email_handler(sender, instance, created, **kwargs):
    """
    Handle email validation (unique)
    """
    email = instance.email
    username = instance.username
    if not email:
        # raise ValidationError('email address required')
        return
    if sender.objects.filter(email=email).exclude(username=username).count() > 0:
        raise ValidationError(_('l\'adresse e-mail existe déjà.'))
