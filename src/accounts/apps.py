from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class AccountsConfig(AppConfig):
    name = 'accounts'
    verbose_name = _('compte utilisateur')
    verbose_name_plural = _('comptes utilisateurs')
