from django.utils.translation import ugettext_lazy as _

from django.contrib import admin
from django.contrib.auth.models import User

from accounts.models.profile import Profile


admin.site.unregister(User)


class UserProfileInline(admin.StackedInline):
    model = Profile

    fieldsets = [
        (None, {'fields': ('birth_date', 'gender', 'photo',)}),
        (_('Contacts'), {'fields': ('phone_number', 'mobile_number', 'fax_number',)}),
        (_('Adresse'), {'fields': ('street_address', 'street_address2', 'postal_code', 'city', 'country',)}),
        (_('Bio'), {'fields': ('bio',)}),
        (_('Administration'), {'fields': ('email_confirmed', 'is_active',)}),
    ]


@admin.register(User)
class UserAdmin(admin.ModelAdmin):

    list_display = ('username', 'first_name', 'last_name', 'email', 'is_active')
    list_filter = ('is_active', 'is_staff')

    fieldsets = [
        (_('Authentification'), {'fields': ('username', 'password', 'first_name', 'last_name', 'email',)}),
        (_('Permissions'), {'fields': ('is_superuser', 'is_staff', 'groups', 'user_permissions',)}),
        (_('Administration'), {'fields': ('is_active', 'date_joined', 'last_login',)}),
    ]

    inlines = [
        UserProfileInline
    ]
