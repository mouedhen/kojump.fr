from django.shortcuts import reverse
from django.views.generic import CreateView

from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm
from django.contrib.auth.views import LoginView, PasswordResetConfirmView, PasswordResetView

from accounts.forms.auth import UserRegistrationForm
from core.mixins.auth import AnonymousRequiredMixin


class UserAuthenticationView(AnonymousRequiredMixin, LoginView):

    form_class = AuthenticationForm
    template_name = 'accounts/authentication.html'


class UserRegistrationView(AnonymousRequiredMixin, CreateView):
    form_class = UserRegistrationForm
    template_name = 'accounts/register.html'

    def get_success_url(self):
        return reverse('accounts:login')


class UserPasswordResetConfirmView(AnonymousRequiredMixin, PasswordResetConfirmView):

    template_name = 'accounts/password_reset_confirm.html'

    def get_success_url(self):
        return reverse('accounts:password_reset_complete')


class UserResetPasswordView(AnonymousRequiredMixin, PasswordResetView):

    form_class = PasswordResetForm
    template_name = 'accounts/password_reset.html'
    email_template_name = 'mails/password_reset_email.html'
    subject_template_name = 'mails/password_reset_subject.txt'

    def get_success_url(self):
        return reverse('accounts:password_reset_done')
