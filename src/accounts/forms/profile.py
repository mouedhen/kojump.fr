from django import forms

from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.models import User

from accounts.models.profile import Profile


class UserForm(UserChangeForm):

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password')


class ProfileForm(forms.ModelForm):

    class Meta:
        model = Profile
        fields = ('phone_number', 'mobile_number', 'fax_number', 'street_address', 'street_address2', 'postal_code',
                  'city', 'country', 'photo', 'birth_date', 'gender', 'bio')
