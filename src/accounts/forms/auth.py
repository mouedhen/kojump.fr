from django import forms
from django.db import transaction
from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class UserRegistrationForm(UserCreationForm):

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2',
                  'street_address', 'street_address2', 'city', 'country')

    first_name = forms.CharField(label=_('prénom'), max_length=30, required=False, )
    last_name = forms.CharField(label=_('nom'), max_length=30, required=False, )
    email = forms.EmailField(label=_('adresse e-mail'), max_length=254, required=False,
                             help_text='merci de fournir une adresse e-mail valide')

    street_address = forms.CharField(label=_('adresse postale'), max_length=254, required=False)
    street_address2 = forms.CharField(label=_('adresse postale (complément)'), max_length=254, required=False)
    city = forms.CharField(label=_('ville'), max_length=100, required=False)
    country = forms.CharField(label=_('pays'), max_length=100, required=False)

    # i_agree = forms.BooleanField(
    #     label=_('en validant, vous acceptez nos conditions d\'utilisation et notre politique de confidentialité.'),
    #     required=True
    # )

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if email and User.objects.filter(email=email).count() > 0:
            raise forms.ValidationError(_('L\'adresse email saisie existe déjà.'))
        return email

    def save(self, commit=True):
        with transaction.atomic():
            user = super(UserRegistrationForm, self).save()
            user.refresh_from_db()
            user.profile.street_address = self.cleaned_data.get('street_address')
            user.profile.street_address2 = self.cleaned_data.get('street_address2')
            user.profile.city = self.cleaned_data.get('city')
            user.profile.country = self.cleaned_data.get('country')
            user.profile.save()
            return user
