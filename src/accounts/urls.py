from django.urls import path, re_path, reverse_lazy

from django.contrib.auth import views as auth_views

from accounts.views import auth as accounts_views
from accounts.views import profile as profile_views

app_name = 'accounts'

urlpatterns = [
    # Accounts connection urls
    path('register/', accounts_views.UserRegistrationView.as_view(), name='register'),
    path('login/', accounts_views.UserAuthenticationView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),

    # Accounts reset password urls
    path('reset/', accounts_views.UserResetPasswordView.as_view(), name='password_reset'),
    path('reset/done/',
         auth_views.PasswordResetDoneView.as_view(template_name='accounts/password_reset_done.html'),
         name='password_reset_done'),
    re_path('^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            accounts_views.UserPasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/complete/',
         auth_views.PasswordResetCompleteView.as_view(template_name='accounts/password_reset_complete.html'),
         name='password_reset_complete'),

    # Accounts profile urls
    path(r'profile/', profile_views.UserProfileView.as_view(), name='profile'),
    path(r'profile/edit/', profile_views.UserProfileEditView.as_view(), name='profile_edit'),

    # Accounts password change
    path('password/',
         auth_views.PasswordChangeView.as_view(
             template_name='profile/password-change.html',
             success_url=reverse_lazy('accounts:password_change_done')),
         name='password_change'),
    path(r'settings/password/done/',
         auth_views.PasswordChangeDoneView.as_view(template_name='profile/password-change-done.html'),
         name='password_change_done'),
]
