from rest_framework import serializers

from equipments.models.categories import Activity


class ActivitySerializer(serializers.ModelSerializer):

    url = serializers.HyperlinkedIdentityField(view_name="api:activity-detail")

    class Meta:
        model = Activity
        fields = ('url', 'label', 'image', 'is_active',)

    # filter_class = SportDisciplineFilter
