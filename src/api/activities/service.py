# -*- coding: utf-8 -*-
from rest_framework import viewsets

from .serializer import ActivitySerializer
from equipments.models.categories import Activity


class ActivityViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Activity.active_objects.distinct()
    serializer_class = ActivitySerializer
