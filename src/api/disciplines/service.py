# -*- coding: utf-8 -*-
from rest_framework import viewsets

from .serializer import SportDisciplineSerializer
from equipments.models.categories import SportDiscipline


class SportDisciplineViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = SportDiscipline.active_objects.distinct()
    serializer_class = SportDisciplineSerializer
