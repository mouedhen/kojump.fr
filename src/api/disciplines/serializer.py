# -*- coding: utf-8 -*-
import django_filters
from rest_framework import serializers

from equipments.models.categories import SportDiscipline
from api.activities.serializer import ActivitySerializer


class SportDisciplineFilter(django_filters.FilterSet):

    class Meta:
        model = SportDiscipline
        fields = ['activity__id', ]


class SportDisciplineSerializer(serializers.ModelSerializer):

    url = serializers.HyperlinkedIdentityField(view_name="api:sportdiscipline-detail")
    activity = ActivitySerializer(read_only=True)

    class Meta:
        model = SportDiscipline
        fields = ('url', 'code', 'label', 'is_active', 'activity')

    filter_class = SportDisciplineFilter
