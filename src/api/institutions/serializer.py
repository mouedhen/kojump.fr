# -*- coding: utf-8 -*-
from rest_framework import serializers
from rest_framework_gis import serializers as gis_serializers
from equipments.models.institution import Institution


class InstitutionSerializer(gis_serializers.GeoFeatureModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="api:institution-detail")
    point = gis_serializers.GeometrySerializerMethodField()

    def get_point(self, obj):
        equipment = obj.equipments.first()
        if equipment:
            return equipment.gps_coordinates
        return None

    class Meta:
        model = Institution
        geo_field = 'point'
        fields = ('url', 'id', 'code', 'image', 'label', 'slug', 'address')
        distinct = True
