# -*- coding: utf-8 -*-
from distutils.util import strtobool

from rest_framework import viewsets
from django_filters import rest_framework as filters

from rest_framework_gis.filters import DistanceToPointFilter
from rest_framework_gis.pagination import GeoJsonPagination

from equipments.models.institution import Institution
from equipments.models.categories import Activity, SportDiscipline, ManagerCategory
from equipments.models.equipment import Equipment

from .serializer import InstitutionSerializer


def disciplines(request):
    if request is None:
        return SportDiscipline.active_objects.distinct()
    d = None
    request_filters = dict(request.GET)
    if 'disciplines' in request_filters:
        d = request_filters['disciplines']
    if not d:
        return SportDiscipline.active_objects.distinct()
    return SportDiscipline.active_objects.filter(code__in=d).distinct()


def activities(request):
    if request is None:
        return Activity.active_objects.distinct()
    a = None
    request_filters = dict(request.GET)
    if 'activities' in request_filters:
        a = request_filters['activities']
    if not a:
        return Activity.active_objects.distinct()
    return Activity.active_objects.filter(id__in=a).distinct()


class InstitutionFilter(filters.FilterSet):
    activity = filters.CharFilter(name='equipments__disciplines__activity_id')
    activities = filters.ModelMultipleChoiceFilter(name='equipments__disciplines__activity', queryset=activities)
    discipline = filters.CharFilter(name='equipments__disciplines__code')
    disciplines = filters.ModelMultipleChoiceFilter(name='equipments__disciplines', queryset=disciplines, )
    department = filters.CharFilter(name='commune__department__code')
    commune = filters.CharFilter(name='commune__code_insee')
    hand_m = filters.BooleanFilter(name='is_accessible_for_hand_m')
    hand_s = filters.BooleanFilter(name='is_accessible_for_hand_s')
    # is_public = filters.BooleanFilter(name='equipments,', method='filter_public',)
    is_public = filters.BooleanFilter(widget=filters.BooleanWidget(), method='filter_public')

    def filter_public(self, queryset, name, value):
        return queryset.filter(equipments__is_public=value)

    class Meta:
        model = Institution
        fields = ['commune']


class InstitutionViewSet(viewsets.ReadOnlyModelViewSet):
    pagination_class = GeoJsonPagination
    # model = Institution
    queryset = Institution.active_objects.filter(is_active=True, equipments__disciplines__is_active=True).distinct()
    serializer_class = InstitutionSerializer
    distance_filter_field = 'equipments__gps_coordinates'
    filter_class = InstitutionFilter
    filter_backends = (DistanceToPointFilter, filters.DjangoFilterBackend)
