# -*- coding: utf-8 -*-
from rest_framework import serializers
from rest_framework_gis import serializers as gis_serializers
from equipments.models.equipment import Equipment


class EquipmentSerializer(gis_serializers.GeoFeatureModelSerializer):

    url = serializers.HyperlinkedIdentityField(view_name="api:equipment-detail")

    class Meta:
        model = Equipment
        geo_field = 'gps_coordinates'
        fields = ('url', 'id', 'code', 'image', 'label', 'slug', 'description',)
        distinct = True
