# -*- coding: utf-8 -*-
from rest_framework import viewsets
from django_filters import rest_framework as filters
from rest_framework_gis.pagination import GeoJsonPagination

from equipments.models.equipment import Equipment

from .serializer import EquipmentSerializer


class EquipmentFilter(filters.FilterSet):
    institution = filters.CharFilter(name='institution__id')

    class Meta:
        model = Equipment
        fields = []


class EquipmentViewSet(viewsets.ReadOnlyModelViewSet):
    pagination_class = GeoJsonPagination
    # TODO change with queryset = Equipment.active_objects.filter(disciplines__is_active=True, ).distinct()
    queryset = Equipment.objects.distinct()
    serializer_class = EquipmentSerializer
    filter_class = EquipmentFilter
    filter_backends = (filters.DjangoFilterBackend,)
