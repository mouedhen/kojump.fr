# -*- coding: utf-8 -*-
from rest_framework import routers

from api.activities.service import ActivityViewSet
from api.disciplines.service import SportDisciplineViewSet

from api.institutions.service import InstitutionViewSet
from api.equipments.service import EquipmentViewSet

app_name = 'api'

router = routers.DefaultRouter()

router.register(r'activities', ActivityViewSet)
router.register(r'disciplines', SportDisciplineViewSet)

router.register(r'institutions', InstitutionViewSet)
router.register(r'equipments', EquipmentViewSet)

urlpatterns = router.urls
