from django.contrib import admin

from publishing.models.publishing import Comment, Favorite, Rate, UserRequest


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):

    save_on_top = True
    list_display = ('comment', 'publisher', 'status', 'content_type', 'object_id', 'created', 'is_active')
    list_filter = ('status', 'is_active', 'created', 'content_type',)
    search_fields = ('publisher__username', 'comment')
    readonly_fields = ('created', 'modified')


@admin.register(Favorite)
class FavoriteAdmin(admin.ModelAdmin):

    save_on_top = True
    list_display = ('publisher', 'content_type', 'object_id', 'created', 'is_active')
    list_filter = ('is_active', 'created', 'content_type',)
    search_fields = ('publisher__username',)
    readonly_fields = ('created', 'modified')


@admin.register(Rate)
class CommentAdmin(admin.ModelAdmin):

    save_on_top = True
    list_display = ('publisher', 'rating', 'content_type', 'object_id', 'created', 'is_active')
    list_filter = ('rating', 'is_active', 'created', 'content_type',)
    search_fields = ('publisher__username',)
    readonly_fields = ('created', 'modified')


@admin.register(UserRequest)
class UserRequestAdmin(admin.ModelAdmin):

    save_on_top = True
    list_display = ('subject', 'content', 'status', 'category', 'obj')
