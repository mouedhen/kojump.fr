from django import template
from django.template import loader

from django.contrib.contenttypes.models import ContentType

from publishing.models.publishing import Favorite

register = template.Library()


class ObjectFavoriteWidget(template.Node):

    def __init__(self, obj):
        self.obj = template.Variable(obj)

    def render(self, context):
        obj = self.obj.resolve(context)
        ct = ContentType.objects.get_for_model(obj)
        is_user_favorite = bool(Favorite.objects.filter(
            publisher=context['request'].user,
            content_type=ct,
            object_id=obj.pk
        ))
        context.push()
        context['object'] = obj
        context["content_type_id"] = ct.pk
        context["is_user_favorite"] = is_user_favorite
        context["count"] = get_favorites_count(obj)
        output = loader.render_to_string("publishing/includes/favorite.html", context.flatten())
        context.pop()
        return output


@register.filter
def get_favorites_count(obj):
    ct = ContentType.objects.get_for_model(obj)
    return Favorite.objects.filter(
        content_type=ct,
        object_id=obj.pk,
    ).count()


@register.tag
def favorite_widget(parser, token):
    try:
        tag_name, for_str, obj = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError(
            "%r tag requires a following syntax : {%% %r for <object> %%}" % (token.content[0], token.content[0])
        )
    return ObjectFavoriteWidget(obj)
