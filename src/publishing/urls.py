# -*- coding: utf-8 -*-
from django.urls import re_path, path

from .views.favorite import json_set_favorite
from .views.comment import CommentOwnerListView


app_name = 'publishing'

urlpatterns = [
    re_path(r'likes/(?P<content_type_id>[^/]+)/(?P<object_id>[^/]+)/$', json_set_favorite, name='json_set_favorite'),
    path('comments/', CommentOwnerListView.as_view(), name='comments-list'),
]
