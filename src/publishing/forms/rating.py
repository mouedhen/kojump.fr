# -*- coding: utf-8 -*-
from django import forms

from publishing.models.publishing import Rate


class RatingForm(forms.ModelForm):
    rating = forms.IntegerField(max_value=5, min_value=1)

    class Meta:
        model = Rate
        fields = ['rating']
