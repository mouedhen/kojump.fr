from django.core.exceptions import PermissionDenied
from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.models import User

from core.models.abstracts import ActivatedAbstractModel, TimeStampedAbstractModel


class ModeratedManager(models.Manager):
    def get_queryset(self):
        return super(ModeratedManager, self).get_queryset().filter(status='YES')


class NotModeratedManager(models.Manager):
    def get_queryset(self):
        return super(NotModeratedManager, self).get_queryset().filter(status='NO')


class PostableManager(models.Manager):
    def get_queryset(self, user):
        return super(PostableManager, self).get_queryset().filter(publisher=user)


class ModeratedAbstractModel(models.Model):
    STATUS = (
        ('NO', _('non modéré')),
        ('YES', _('modéré')),
    )

    class Meta:
        abstract = True

    status = models.CharField(_('statut'), max_length=3, choices=STATUS, default='NO')

    objects = models.Manager()
    moderated = ModeratedManager()
    not_moderated = NotModeratedManager()


class PostableAbstractModel(ActivatedAbstractModel, TimeStampedAbstractModel):
    class Meta:
        abstract = True

    publisher = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)

    def set_publisher(self, user):
        if not user:
            raise PermissionDenied
        self.publisher = user

    objects = models.Manager()
    publications = PostableManager()
