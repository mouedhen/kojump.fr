from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.models.abstracts import GenericRelationAbstractModel

from .abstracts import ModeratedAbstractModel, PostableAbstractModel


class Comment(GenericRelationAbstractModel, ModeratedAbstractModel, PostableAbstractModel):
    class Meta:
        verbose_name = _('commentaire')
        verbose_name_plural = _('commentaires')

    comment = models.TextField(_('commentaire'))

    def __str__(self):
        return self.comment


class Favorite(GenericRelationAbstractModel, PostableAbstractModel):
    class Meta:
        verbose_name = _('favori')
        verbose_name_plural = _('favoris')

    def __str__(self):
        return self.publisher.username


class Rate(GenericRelationAbstractModel, PostableAbstractModel):
    RATING_CHOICES = (
        (1, _('très mauvais')),
        (2, _('mauvais')),
        (3, _('moyen')),
        (4, _('bon')),
        (5, _('très bon')),
    )

    class Meta:
        verbose_name = _('note')
        verbose_name_plural = _('notes')

    rating = models.IntegerField(_('note'), choices=RATING_CHOICES)

    def __str__(self):
        return self.get_rating_display()


class UserRequest(GenericRelationAbstractModel, PostableAbstractModel):

    STATUS_CHOICE = (
        ('WAITING', _('en attente')),
        ('TREATED', _('traitée')),
        ('FAILURE', _('refusée')),
    )

    SUBJECT_CATEGORIES = (
        ('PROPERTY', _('changement de propriétaire')),
        ('CLAIM', _('réclamation')),
        ('OTHERS', _('autres')),
    )

    REQUEST_OBJECT = (
        ('INSTITUTION', _('institution sportive')),
    )

    class Meta:
        verbose_name = _('requête utilisateur')
        verbose_name_plural = _('requêtes utilisateurs')

    subject = models.CharField(_('sujet'), max_length=100)
    content = models.TextField(_('conten'))
    status = models.CharField(_('statut'), max_length=7, choices=STATUS_CHOICE, default='WAITING')
    category = models.CharField(_('catégorie'), max_length=8, choices=SUBJECT_CATEGORIES, default='PROPERTY')
    obj = models.CharField(_('objet'), max_length=14, choices=REQUEST_OBJECT, default='INSTITUTION')

    def __str__(self):
        return self.subject
