from django.utils.translation import ugettext_lazy as _
from django.apps import AppConfig


class PublishingConfig(AppConfig):
    name = 'publishing'
    verbose_name = _('publications')
