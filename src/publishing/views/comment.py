from django.views.generic import ListView

from django.contrib.auth.mixins import LoginRequiredMixin

from publishing.models.publishing import Comment


class CommentOwnerListView(LoginRequiredMixin, ListView):

    model = Comment
    template_name = 'publishing/comments-list.html'

    def get_queryset(self):
        return Comment.objects.filter(publisher=self.request.user).distinct()
