import json

from django.http import HttpResponse
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt

from django.contrib.contenttypes.models import ContentType

from publishing.models.publishing import Favorite
from publishing.templatetags.favorites_tags import get_favorites_count


@never_cache
@csrf_exempt
def json_set_favorite(request, content_type_id, object_id):
    """
    Set the object as favorite for the current user
    """
    result = {
        'success': False,
    }
    if request.user.is_authenticated and request.method == 'POST':
        content_type = ContentType.objects.get(id=content_type_id)
        obj = content_type.get_object_for_this_type(pk=object_id)
        favorite, is_created = Favorite.objects.get_or_create(
            content_type=ContentType.objects.get_for_model(obj),
            object_id=obj.id,
            publisher=request.user
        )
        if not is_created:
            favorite.delete()
        result = {
            'success': True,
            'obj': obj.label,
            'action': is_created and 'added' or 'removed',
            'count': get_favorites_count(obj)
        }
    data = json.dumps(result, ensure_ascii=False)
    return HttpResponse(data)
