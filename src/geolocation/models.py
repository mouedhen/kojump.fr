# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.contrib.gis.db import models

from core.models.abstracts import LocatedAbstractModel


@python_2_unicode_compatible
class GeoLocationAbstractModel(models.Model):

    gps_coordinates = models.PointField(_('coordonnées GPS'))

    class Meta:
        verbose_name = _('emplacement géographique')
        verbose_name_plural = _('emplacements géographiques')
        abstract = True

    def __str__(self):
        return str(self.gps_coordinates)
